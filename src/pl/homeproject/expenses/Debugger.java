package pl.homeproject.expenses;

public class Debugger {

	public static void nullCheck(String label, Object variable){
		System.err.println(label + " is null " + (variable == null ? "yes" : "no"));
	}
}
