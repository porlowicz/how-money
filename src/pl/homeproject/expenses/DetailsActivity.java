package pl.homeproject.expenses;

import java.util.Calendar;
import java.util.List;

import pl.homeproject.expenses.fragment.DateHost;
import pl.homeproject.expenses.fragment.DatePickerFragment;
import pl.homeproject.expenses.handler.KeyboardHider;
import pl.homeproject.expenses.handler.RollExpensesHandler;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.popupWindow.AbstractPopupWindow;
import pl.homeproject.expenses.popupWindow.AddCommentPopupWindow;
import pl.homeproject.expenses.popupWindow.DiscardExpenseInputWarningPopup;
import pl.homeproject.expenses.popupWindow.ModifyCategoryNamePopupWindow;
import pl.homeproject.expenses.popupWindow.ModifyExpensePopupWindow;
import pl.homeproject.expenses.register.TileFactory;
import pl.homeproject.expenses.register.data.Comment;
import pl.homeproject.expenses.register.data.DateFormat;
import pl.homeproject.expenses.register.data.DetailsProvider;
import pl.homeproject.expenses.register.data.ExpenseData;
import pl.homeproject.expenses.view.DecimalEditText;
import pl.homeproject.expenses.view.DecimalTextView;
import pl.homeproject.expenses.view.DetailView;
import pl.homeproject.expenses.view.RollUpDownImageView;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class DetailsActivity extends ActionBarActivity implements
		OnClickListener, DateHost {

	private Calendar periodShown = null;
	private Persister persister;
	private Category category;
	private List<DetailView> rows;
	private Comment comment = new Comment();
	private ModifyExpensePopupWindow popupWindow;

	private RollExpensesHandler rollExpensesHandler = new RollExpensesHandler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		persister = Persister.getInstance(this);
		periodShown = Calendar.getInstance();
		category = (Category) getIntent().getSerializableExtra("category");

		if (isDateChanged()) {
			updateLimitsIfNecessary();
		}

		showDetails();
		updateActionBar();
		getSupportActionBar().setHomeButtonEnabled(true);

	}

	@Override
	public void onBackPressed() {

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			DecimalEditText expenseAmountDetail = (DecimalEditText) findViewById(R.id.expenseAmountDetail);
			String amountText = expenseAmountDetail.getText().toString();
			if (amountText == null || amountText.length() == 0) {
				super.onBackPressed();
			} else {
				DiscardExpenseInputWarningPopup discardExpenseInputWarningPopup = new DiscardExpenseInputWarningPopup(
						this, expenseAmountDetail);
				discardExpenseInputWarningPopup.show();
			}
		} else {
			super.onBackPressed();
		}
	}

	private void updateLimitsIfNecessary() {
		persister.propagateLimitToNextMonthIfNecessary(category, periodShown);
	}

	private boolean isDateChanged() {
		Calendar today = Calendar.getInstance();
		if (today.get(Calendar.DATE) == 1) {
			return true;
		}
		return false;
	}

	private void updateActionBar() {
		getSupportActionBar().setTitle(
				String.format("%-20s%s", category.getLimitedCategoryName(15),
						DateFormat.formatDate(periodShown.getTime())));
	}

	public void showDetails() {
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			showDetails(R.layout.activity_details);
		} else {
			showDetails(R.layout.activity_details_landscape);
		}
	}

	private void showDetails(int layoutId) {
		setContentView(layoutId);
		WidgetConfig.updateWidgets(DetailsActivity.this);

		final RollUpDownImageView rollUpDownBudgetImage = (RollUpDownImageView) findViewById(R.id.rollUpDownBudgetImage);
		final View rollUpSummaryTextView = findViewById(R.id.rollUpSummaryTextView);
		rollUpSummaryTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (rollUpDownBudgetImage.isUp()) {
					rollDown(v.getRootView());
				} else if (rollUpDownBudgetImage.isUpHalf()) {
					rollUp(v.getRootView());
				} else {
					rollHalfUp(v.getRootView());
				}
				rollUpDownBudgetImage.nextState();
			}

			private void rollHalfUp(View v) {
				ViewGroup group = (ViewGroup) v.findViewById(R.id.detailRows);
				for (int i = 0; i < group.getChildCount(); i++) {
					ViewGroup child = (ViewGroup) group.getChildAt(i);
					if (child.getChildAt(0).getId() == R.id.detailsTopRowGridLayout) {
						rollExpensesHandler.rollDown(child
								.findViewById(R.id.detailTopRowAmountLayout),
								(DetailView) child, group, i);
					}
				}
				View detailRows = v.findViewById(R.id.detailsScrollView);
				detailRows.setVisibility(View.VISIBLE);
				v.findViewById(R.id.rollUpSummaryRow).setVisibility(View.GONE);
				rollUpDownBudgetImage.setImageResource(R.drawable.up_half);
			}

			private void rollUp(View v) {
				ViewGroup group = (ViewGroup) v.findViewById(R.id.detailRows);
				int sum = 0;
				for (int i = 0; i < group.getChildCount(); i++) {
					ViewGroup child = (ViewGroup) group.getChildAt(i);
					if (child.getChildAt(0).getId() != R.id.detailsTopRowGridLayout) {
						DetailView detailView = (DetailView) child;
						sum += detailView.getDetailData().getAmount();
					} else if (i + 1 == group.getChildCount()
							|| ((ViewGroup) group.getChildAt(i + 1))
									.getChildAt(0).getId() == R.id.detailsTopRowGridLayout) {
						DetailView detailView = (DetailView) child;
						sum += detailView.getDetailData().getAmount();
					}
				}
				View detailRows = v.findViewById(R.id.detailsScrollView);
				detailRows.setVisibility(View.GONE);
				v.findViewById(R.id.rollUpSummaryRow).setVisibility(
						View.VISIBLE);
				DecimalTextView detailAmount = (DecimalTextView) v
						.findViewById(R.id.rollUpSummaryRow).findViewById(
								R.id.detailAmount);
				TextView detailDate = (TextView) v.findViewById(
						R.id.rollUpSummaryRow).findViewById(R.id.detailDate);
				detailDate.setText(DateFormat.formatPeriod(periodShown
						.getTime()));
				detailAmount.setText(String.valueOf(sum));
				rollUpDownBudgetImage.setImageResource(R.drawable.up);
			}

			private void rollDown(View v) {
				ViewGroup group = (ViewGroup) v.findViewById(R.id.detailRows);
				for (int i = 0; i < group.getChildCount(); i++) {
					ViewGroup child = (ViewGroup) group.getChildAt(i);
					if (child.getChildAt(0).getId() == R.id.detailsTopRowGridLayout) {
						rollExpensesHandler.rollUp(child
								.findViewById(R.id.detailTopRowAmountLayout),
								(DetailView) child, group, i);
					}
				}
				View detailRows = v.findViewById(R.id.detailsScrollView);
				detailRows.setVisibility(View.VISIBLE);
				v.findViewById(R.id.rollUpSummaryRow).setVisibility(View.GONE);
				rollUpDownBudgetImage.setImageResource(R.drawable.down);
			}
		});

		View changeDateLinearLayout = findViewById(R.id.changeDateLinearLayout);
		changeDateLinearLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment newFragment = new NewDetailsDatePickerFragment(
						DetailsActivity.this);
				newFragment.show(getSupportFragmentManager(), "datePicker");
			}
		});

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			setUpPortraitHeader();
		}

		Balance balance = persister.findBalanceFor(category, periodShown);
		int color = TileFactory.gradientFor(balance);
		TextView detailsBalance = (TextView) findViewById(R.id.detailsBalance);
		detailsBalance.setTextColor(color);

		TextView periodHint = (TextView) findViewById(R.id.periodHint);
		periodHint.setText(DateFormat.formatPeriod(periodShown.getTime()));

		DetailsProvider detailsProvider = new DetailsProvider(this);
		rows = detailsProvider.getDetailsFor(category, periodShown);

		ViewGroup rowsContainer = (ViewGroup) findViewById(R.id.detailRows);
		rowsContainer.removeAllViews();
		for (DetailView detailView : rows) {
			rowsContainer.addView(detailView);
		}

		View previousPeriod = findViewById(R.id.previousPeriod);
		View nextPeriod = findViewById(R.id.nextPeriod);
		previousPeriod.setOnClickListener(this);
		nextPeriod.setOnClickListener(this);

		View graphStatusEmpty = findViewById(R.id.graphStatusEmpty);
		View graphStatusSpent = findViewById(R.id.graphStatusSpent);

		int balanceValue = balance.getBalance();
		int limitValue = balance.getLimit();

		LinearLayout.LayoutParams emptyParams;
		LinearLayout.LayoutParams spentParams;
		if (balanceValue <= 0) {
			emptyParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.MATCH_PARENT, 0);
			spentParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.MATCH_PARENT, 1);
		} else {
			emptyParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.MATCH_PARENT, balanceValue);
			spentParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.MATCH_PARENT, limitValue - balanceValue);
		}
		graphStatusSpent.setBackgroundColor(color);
		graphStatusSpent.setLayoutParams(spentParams);
		graphStatusEmpty.setLayoutParams(emptyParams);

		View showBarCharButton = findViewById(R.id.showBarCharButton);
		showBarCharButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent graphIntent = new Intent(DetailsActivity.this,
						GraphActivity.class);
				graphIntent.putExtra("category", category);
				DetailsActivity.this.startActivity(graphIntent);
			}
		});
	}

	private void setUpPortraitHeader() {

		View addCommentButton = findViewById(R.id.addCommentButton);
		addCommentButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AddCommentPopupWindow(DetailsActivity.this, comment).show();
			}
		});

		View addSaveButton = findViewById(R.id.addSaveButton);
		addSaveButton.setOnClickListener(new OnClickListener() {
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				saveExpense();
			}
		});

		final EditText expenseAmountDetail = (EditText) findViewById(R.id.expenseAmountDetail);
		expenseAmountDetail.setImeActionLabel("Save", EditorInfo.IME_ACTION_GO);
		expenseAmountDetail
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {

						if (actionId == EditorInfo.IME_ACTION_GO) {
							saveExpense();
							return true;
						}

						return false;
					}
				});
		final CharSequence hintText = expenseAmountDetail.getHint();
		expenseAmountDetail
				.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							expenseAmountDetail.setGravity(Gravity.RIGHT
									| Gravity.CENTER_VERTICAL);
							expenseAmountDetail.setHint("");
						} else {
							expenseAmountDetail.setHint(hintText);
						}
					}
				});
		expenseAmountDetail.clearFocus();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.details_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.change_current_date:
			DialogFragment newFragment = new NewDetailsDatePickerFragment(this);
			newFragment.show(getSupportFragmentManager(), "datePicker");
			return true;
		case R.id.change_category_name:
			AbstractPopupWindow popupWindow = new ModifyCategoryNamePopupWindow(
					this, category);
			popupWindow.show();
			return true;
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.previousPeriod) {
			periodShown.add(Calendar.MONTH, -1);
		} else if (v.getId() == R.id.nextPeriod) {
			periodShown.add(Calendar.MONTH, 1);
		}
		updateActionBar();
		showDetails();
	}

	@Override
	public void setDate(Calendar date) {
		boolean monthChanged = monthChanged(date, periodShown);

		periodShown = date;

		updateActionBar();

		TextView periodHint = (TextView) findViewById(R.id.periodHint);
		periodHint.setText(DateFormat.formatPeriod(periodShown.getTime()));

		if (monthChanged) {
			showDetails();
		}
	}

	private boolean monthChanged(Calendar date, Calendar periodShown2) {
		return date.get(Calendar.MONTH) != periodShown2.get(Calendar.MONTH)
				|| date.get(Calendar.YEAR) != periodShown2.get(Calendar.YEAR);
	}

	@Override
	public Calendar getDate() {
		return periodShown;
	}

	public void updateCategory(String newCategory) {
		category = new Category(category.getId(), newCategory,
				category.getOrdinalNumber());
		updateActionBar();
	}

	@SuppressLint("NewApi")
	private void saveExpense() {
		DecimalEditText expenseAmountDetail = (DecimalEditText) DetailsActivity.this
				.findViewById(R.id.expenseAmountDetail);
		int expenseAmountValue = expenseAmountDetail.getFormattedText();
		expenseAmountDetail.setText("");

		ExpenseData expense = new ExpenseData(-1, category, periodShown,
				expenseAmountValue, comment.getContent());
		comment.setContent("");
		persister.save(expense);

		View detailAmount = DetailsActivity.this
				.findViewById(R.id.detailAmount);
		int height = 1;
		if (detailAmount != null) {
			height = detailAmount.getHeight();
		}

		DetailsActivity.this.showDetails();

		final ScrollView detailsScrollView = (ScrollView) DetailsActivity.this
				.findViewById(R.id.detailsScrollView);

		final int viewIndex = findScrollHeight();
		final int viewHeight = height;

		if (android.os.Build.VERSION.SDK_INT >= 11) {
			if (viewIndex >= 0 && viewIndex < rows.size()) {
				View highlightedRow = rows.get(viewIndex);
				ObjectAnimator animator = ObjectAnimator.ofObject(
						highlightedRow, "backgroundColor", new ArgbEvaluator(),
						0x33ffffff, 0x00ffffff);
				animator.setDuration(2000);
				animator.start();
			}
		}
		detailsScrollView.post(new Runnable() {
			@Override
			public void run() {
				detailsScrollView.scrollBy(0, viewIndex * viewHeight);
			}
		});
		KeyboardHider.hide(DetailsActivity.this,
				DetailsActivity.this.findViewById(R.id.detailsRootView));
	}

	private int findScrollHeight() {
		int i = 0;
		for (; i < rows.size(); i++) {
			if (rows.get(i).getDetailData().getDetailDate().after(periodShown)) {
				return i - 1;
			}
		}
		return i - 1;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			showDetails(R.layout.activity_details_landscape);
			if (popupWindow != null) {
				if (popupWindow.isShowing()) {
					popupWindow.adjustDeleteButtonTo(Configuration.ORIENTATION_LANDSCAPE);
				}
			}
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			showDetails();
			if (popupWindow != null) {
				if (popupWindow.isShowing()) {
					popupWindow.adjustDeleteButtonTo(Configuration.ORIENTATION_PORTRAIT);
				}
			}
		}
	}

	public void addPopupWindow(ModifyExpensePopupWindow popupWindow) {
		this.popupWindow = popupWindow;
	}

}

class NewDetailsDatePickerFragment extends DatePickerFragment {

	private DetailsActivity activity;

	public NewDetailsDatePickerFragment(DetailsActivity activity) {
		this.activity = activity;
	}

	@Override
	protected DateHost getDateHost() {
		return activity;
	}

}
