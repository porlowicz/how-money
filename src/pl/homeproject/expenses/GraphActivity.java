package pl.homeproject.expenses;

import java.util.Calendar;
import java.util.List;

import org.achartengine.GraphicalView;

import pl.homeproject.expenses.handler.graph.BarChartHolder;
import pl.homeproject.expenses.handler.graph.PieChartHolder;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.register.TileFactory;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;

public class GraphActivity extends ActionBarActivity  {
	
	private int choosenYear = Calendar.getInstance().get(Calendar.YEAR);
	
	private PieChartHolder pieChartHolder;

	private TileFactory manager;
	private View pieChartView;
	private boolean timeseriesFromBudget;

	private Category category;
	private boolean isFromNewControlFlow = false;
	
	private Category budget;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graph);
		getSupportActionBar().setHomeButtonEnabled(true);

		hideAllGraphs();
		
		manager = new TileFactory(this);
		
		budget = (Category) getIntent().getSerializableExtra("budget");
		category = (Category) getIntent().getSerializableExtra("category");
		
		List<TileViewModel> tiles;
		if(budget != null){
			findViewById(R.id.graphDetails).setVisibility(View.VISIBLE);
			
			
			isFromNewControlFlow = true;
			tiles = manager.getTileViewsFor(budget);
			pieChartHolder = new PieChartHolder(this, tiles);
			pieChartView = pieChartHolder.createPieChartView(budget.getCategory(), true);
			
			ViewGroup graphDetails = (ViewGroup) findViewById(R.id.graphDetails);
			graphDetails.removeViewAt(0);
			graphDetails.addView(pieChartView, 0);

			setTimeseriesFromBudget(true);
			
		}else if(category != null){
			findViewById(R.id.graphTimeseries).setVisibility(View.VISIBLE);
			
			isFromNewControlFlow = true;
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}else{
			findViewById(R.id.graph).setVisibility(View.VISIBLE);
			
			tiles = manager.getBudgetTiles();
			tiles.addAll(manager.getTileViews());
			pieChartHolder = new PieChartHolder(this, tiles);
			pieChartView = pieChartHolder.createPieChartView("", false);
			
			ViewGroup graph = (ViewGroup) findViewById(R.id.graph);
			graph.removeViewAt(0);
			graph.addView(pieChartView, 0);
		}
	}

	private void hideAllGraphs() {
		findViewById(R.id.graph).setVisibility(View.INVISIBLE);
		findViewById(R.id.graphDetails).setVisibility(View.INVISIBLE);
		findViewById(R.id.graphTimeseries).setVisibility(View.INVISIBLE);
		findViewById(R.id.selectedPieHint).sendAccessibilityEvent(View.INVISIBLE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		
		if(findViewById(R.id.graph).getVisibility() == View.VISIBLE){
			super.onBackPressed();
		}else if(findViewById(R.id.graphDetails).getVisibility() == View.VISIBLE){
			if(isFromNewControlFlow){
				super.onBackPressed();
				finish();
				return;
			}
			findViewById(R.id.graphDetails).setVisibility(View.GONE);
			findViewById(R.id.graph).setVisibility(View.VISIBLE);
			pieChartHolder.setOnClickListeners();
			findViewById(R.id.graphLegend).setVisibility(View.VISIBLE);
		}else{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		findViewById(R.id.selectedPieHint).setVisibility(View.GONE);
		findViewById(R.id.graphsMenuButtons).setVisibility(View.GONE);
	}

	public void setTimeseriesFromBudget(boolean timeseriesFromBudget) {
		this.timeseriesFromBudget = timeseriesFromBudget;
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	
	    	getSupportActionBar().hide();
	    	
	        ViewGroup graphTimeseries = createTimeseriesChartForYear(Calendar.getInstance().get(Calendar.YEAR));
	        
	        View changeYearOnChartButton = graphTimeseries.findViewById(R.id.change_year_on_chart_button);
	        changeYearOnChartButton.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi")
				@Override
				public void onClick(View v) {

					final Dialog datePickerDialog = new Dialog(GraphActivity.this);
					datePickerDialog.setTitle("Choose year");
					datePickerDialog.setContentView(R.layout.year_number_picker);
					datePickerDialog.setCancelable(true);
					
					Button button1Set = (Button) datePickerDialog.findViewById(R.id.button1_set);
					Button button2Cancel = (Button) datePickerDialog.findViewById(R.id.button2_cancel);
					
					if(android.os.Build.VERSION.SDK_INT >= 11){
						final NumberPicker np = (NumberPicker) datePickerDialog.findViewById(R.id.numberPicker1);
				        np.setMaxValue(4000);
				        np.setMinValue(1900);
				        np.setValue(choosenYear);
				        np.setWrapSelectorWheel(false);
				        
						button1Set.setOnClickListener(new OnClickListener(){
							@Override
							public void onClick(View v) {
								int year = np.getValue();
								choosenYear = year;
								createTimeseriesChartForYear(year);
								datePickerDialog.dismiss();
							}
	
						});
						button2Cancel.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								datePickerDialog.dismiss();
							}
						});		
						}
						datePickerDialog.show();
				}
			});
	        
	        setTimeseriesFromBudget(findViewById(R.id.graphDetails).getVisibility() == View.VISIBLE);
	        findViewById(R.id.graph).setVisibility(View.GONE);
	        findViewById(R.id.graphDetails).setVisibility(View.GONE);
	        findViewById(R.id.selectedPieHint).setVisibility(View.GONE);
	        graphTimeseries.setVisibility(View.VISIBLE);
	        getSupportActionBar().setTitle(category.getCategory());
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	getSupportActionBar().show();
	    	
			findViewById(R.id.graphTimeseries).setVisibility(View.GONE);
			if(timeseriesFromBudget){
				findViewById(R.id.graphDetails).setVisibility(View.VISIBLE);
				findViewById(R.id.graph).setVisibility(View.GONE);
				findViewById(R.id.graphDetailsLegend).setVisibility(View.VISIBLE);
			}else{
				findViewById(R.id.graphDetails).setVisibility(View.GONE);
				findViewById(R.id.graph).setVisibility(View.VISIBLE);
				findViewById(R.id.graphLegend).setVisibility(View.VISIBLE);
				if(isFromNewControlFlow){
					finish();
				}
			}
	    }
	}

	private ViewGroup createTimeseriesChartForYear(int year) {
		BarChartHolder lineChartHolder = new BarChartHolder(this, category, year);
		GraphicalView lineChartView = lineChartHolder.createXYChart();
		
		ViewGroup graphTimeseries = (ViewGroup) findViewById(R.id.graphTimeseries);
		graphTimeseries.removeViewAt(0);
		graphTimeseries.addView(lineChartView, 0);
		return graphTimeseries;
	}

	public void setTimeseriesCategory(Category category) {
		this.category = category;		
	}

}


