package pl.homeproject.expenses;

import java.util.Calendar;

import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import pl.homeproject.expenses.view.helper.StringControl;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.RemoteViews;

public class HowMoneyWidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);

		for (int widgetId : appWidgetIds) {
			
			SharedPreferences sharedPreferences = context.getSharedPreferences(WidgetConfig.SHARED_PREFERENCES_NAME, 0);
			
			int categoryId = sharedPreferences.getInt(String.valueOf(widgetId), 0);
			
			Persister persister = Persister.getInstance(context);
			
			Category category = persister.findCategory(categoryId);
			if(category == null){
				RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.howmoney_widget);	
				views.setViewVisibility(R.id.widgetCategoryName, View.INVISIBLE);
				views.setTextViewText(R.id.widgetBudgetName, "Category does not exist");
				views.setTextViewText(R.id.widgetMoneyLeft, "n/a");	
				appWidgetManager.updateAppWidget(widgetId, views);
				continue;
			}
			
			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.howmoney_widget);
			
			Balance balance = persister.findBalanceFor(category, Calendar.getInstance());
			
			int moneyLeft = balance.getLimit() - balance.getDebit();
			
			Category data = category;
			if(data.getParentCategory() != null){
				views.setTextViewText(R.id.widgetBudgetName, StringControl.cut(data.getParentCategory().getCategory(), 10) + ":");
				views.setTextViewText(R.id.widgetCategoryName, StringControl.cut(data.getCategory(), 10));
			}else{
				views.setViewVisibility(R.id.widgetCategoryName, View.INVISIBLE);
				views.setTextViewText(R.id.widgetBudgetName, StringControl.cut(data.getCategory(), 10));
			}
			views.setTextViewText(R.id.widgetMoneyLeft, DecimalFormatter.formatDecimalNumber(String.valueOf(moneyLeft)));
			
			Intent defineIntent = new Intent(context, DetailsActivity.class);
			defineIntent.putExtra("category", data);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, widgetId, defineIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
			views.setOnClickPendingIntent(R.id.homescreenWidgetTopView, pendingIntent);
			// Tell the widget manager
			appWidgetManager.updateAppWidget(widgetId, views);
			
		}

	}

}
