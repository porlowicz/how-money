package pl.homeproject.expenses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.achartengine.GraphicalView;
import org.achartengine.renderer.DefaultRenderer;

import pl.homeproject.expenses.fragment.DateHost;
import pl.homeproject.expenses.handler.AddNewTileListener;
import pl.homeproject.expenses.handler.DateChangedListener;
import pl.homeproject.expenses.handler.graph.PieChartFactory;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.register.TileFactory;
import pl.homeproject.expenses.register.TilesViewManager;
import pl.homeproject.expenses.view.CategoryLimitedLengthTextView;
import pl.homeproject.expenses.view.GraphFrameLayoutAdapter;
import pl.homeproject.expenses.view.TileView;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements DateHost, MediaScannerConnectionClient {

	public static final int DEFAULT_BUDGET_TYPE_ID = 1;

	private Calendar date;

	private List<DateChangedListener> dateChangedListeners = new ArrayList<DateChangedListener>();
 
	public int getBudgetTypeId() {
		return DEFAULT_BUDGET_TYPE_ID;
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		showTiles();

		View tilesGrid = findViewById(R.id.tilesGrid);
		tilesGrid.setClickable(true);
		View topLevelView = findViewById(R.id.topLevelView);
		topLevelView.setClickable(true);

		if (android.os.Build.VERSION.SDK_INT >= 11){
			findViewById(R.id.scrollUpHelper).setTranslationY(-25);
			findViewById(R.id.scrollDownHelper).setTranslationY(25);
		}
	}

	public void removeCategory(View view) {
		TileView tileTemplate = (TileView) view
				.findViewById(R.id.tileTemplateView);
		CategoryLimitedLengthTextView tileCategory = (CategoryLimitedLengthTextView) view.findViewById(R.id.tileCategory);
		if (!tileTemplate.isBudget()) {
			Persister.getInstance(this).deleteCategory(
					tileCategory.getCategory());
		} else {
			Persister.getInstance(this)
					.deleteBudget(tileCategory.getCategory());
		}
		WidgetConfig.updateWidgets(this);
	}

	public void addSettingsTile() {
		View settingsTile = getLayoutInflater().inflate(
				R.layout.settings_tile_layout, null);
		settingsTile.findViewById(R.id.addTileTemplateView).setOnClickListener(new AddNewTileListener(this));
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		tilesGrid.addView(settingsTile);
	}

	public void replaceAddTileWithDelete(){

		View deleteTileLayout = getLayoutInflater().inflate(R.layout.delete_tile_layout, null);
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		View addRemoveSetting = findViewById(R.id.addRemoveSetting);
		tilesGrid.removeView(addRemoveSetting);
		tilesGrid.addView(deleteTileLayout);
		View showChartTile = tilesGrid.findViewById(R.id.showChartTile);
		tilesGrid.removeView(showChartTile);
		addChartTile();
	}
	
	public void replaceDeleteWithAddTile(){
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		if(tilesGrid.getChildAt(tilesGrid.getChildCount()-1) instanceof GraphFrameLayoutAdapter){
			tilesGrid.removeViewAt(tilesGrid.getChildCount()-1);
		}
		tilesGrid.removeViewAt(tilesGrid.getChildCount()-1);
		addSettingsTile();
		addChartTile();
	}

	public void addChartTile() {
		ViewGroup chartTile = (ViewGroup) getLayoutInflater().inflate(
				R.layout.chart_tile_layout, null);

		TileFactory manager = new TileFactory(this);
		List<TileViewModel> tiles = manager.getBudgetTiles();
		tiles.addAll(manager.getTileViews());

		double sum = 0;
		List<TileViewModel> filteredTiles = new LinkedList<TileViewModel>();
		for (TileViewModel tileViewModel : tiles) {
			sum += tileViewModel.getDebit();
			if(tileViewModel.getDebit() != 0){
				filteredTiles.add(tileViewModel);
			}
		}

		GraphicalView pieChart = new PieChartFactory(this,
				new DefaultRenderer()).createPieChartThumbnail(filteredTiles, sum);
		if(pieChart == null){
			return;
		}
		ViewGroup innerShowChartTile = (ViewGroup) chartTile.findViewById(R.id.innerShowChartTile);
		innerShowChartTile.addView(pieChart);

		chartTile.findViewById(R.id.innerShowChartTile).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent graphIntent = new Intent(MainActivity.this, GraphActivity.class);
				MainActivity.this.startActivity(graphIntent);
			}
		});
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		tilesGrid.addView(chartTile);
	}

	protected void showTiles() {
		TilesViewManager manager = new TilesViewManager(this);
		List<View> tiles = manager.getMainTiles();

		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		for (View tileView : tiles) {
			tilesGrid.addView(tileView);
		}
		addSettingsTile();
		addChartTile();
		if(android.os.Build.VERSION.SDK_INT < 11){
			findViewById(R.id.dndTilesGrid).requestLayout();
		}
	}

	public void updateTiles() {
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		tilesGrid.removeAllViews();
		showTiles();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.actionbar_preferences:
			Intent intent = new Intent(this, PreferencesActivity.class);
			this.startActivity(intent);
			return true;
		case R.id.show_charts_menu:
			Intent graphIntent = new Intent(this, GraphActivity.class);
			this.startActivity(graphIntent);
			return true;
//		case R.id.actionbar_export_data:
//			exportData();
//			return super.onOptionsItemSelected(item);
//		case R.id.actionbar_import_data:
//			importData();
//			return super.onOptionsItemSelected(item);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void importData() {
		if(isExternalStorageWritable()){
			 file = new File(DB_BAK_FILE, DB_BAK_FILENAME);
			 try {
				if(!file.exists()){
					file.createNewFile();
				}
				copyFile(new FileInputStream(file), new FileOutputStream(DB_FILEPATH));
				conn = new MediaScannerConnection(this, this);
				conn.connect();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			Toast.makeText(this, "There is no external storage available", Toast.LENGTH_LONG).show();
		}
	}

	private final static String DB_FILEPATH = "/data/data/pl.homeproject.expenses/databases/db_expenses";
	private final static File DB_BAK_FILE = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS);
	private final static String DB_BAK_FILENAME = "HowMoney.db";
	
	private File file;
	private MediaScannerConnection conn;
	private void exportData() {
		if(isExternalStorageWritable()){
			 file = new File(DB_BAK_FILE, DB_BAK_FILENAME);
			 try {
				if(!file.exists()){
					file.createNewFile();
				}
				copyFile(new FileInputStream(DB_FILEPATH), new FileOutputStream(file));
				conn = new MediaScannerConnection(this, this);
				conn.connect();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			Toast.makeText(this, "There is no external storage available", Toast.LENGTH_LONG).show();
		}
	}
	
    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
	
	public boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateTiles();
	}

	@Override
	public Calendar getDate() {
		return date;
	}

	@Override
	public void setDate(Calendar date) {
		this.date = date;
		notifyListeners();
	}

	private void notifyListeners() {
		for (DateChangedListener listener : dateChangedListeners) {
			listener.updateDate(date);
		}
	}

	public void addDateChangedListener(DateChangedListener popupWindow) {
		dateChangedListeners.add(popupWindow);
	}

	public void removeDateChangedListener(DateChangedListener popupWindow) {
		dateChangedListeners.remove(popupWindow);
	}

	@Override
	public void onMediaScannerConnected() {
		conn.scanFile(file.getPath(), null);
	}

	@Override
	public void onScanCompleted(String path, Uri uri) {
		conn.disconnect();
		conn = null;
	}

}
