package pl.homeproject.expenses;

import pl.homeproject.expenses.handler.KeyboardHider;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.view.CurrencySpinner;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

public class PreferencesActivity extends ActionBarActivity implements OnClickListener{

	private View preferencesCancelButton;
	private View preferencesSaveButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setTitle("Preferences");
		
		preferencesCancelButton = findViewById(R.id.preferencesCancelButton);
		preferencesSaveButton = findViewById(R.id.preferencesSaveButton);
		preferencesCancelButton.setOnClickListener(this);
		preferencesSaveButton.setOnClickListener(this);
		
		final EditText currencySymbol = (EditText) findViewById(R.id.currencySymbol);
		String symbol = Preferences.getCurrencySymbol(this);
		currencySymbol.setText(symbol.trim());
		currencySymbol.setSelection(currencySymbol.getText().toString().length());
		
		currencySymbol.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				KeyboardHider.hide(PreferencesActivity.this, currencySymbol);
				
				Spinner spinner = new CurrencySpinner(PreferencesActivity.this, (EditText) v);

				ViewGroup parent = (ViewGroup) v.getParent();
				int indexOfChild = parent.indexOfChild(v);
				parent.removeView(v);
				parent.addView(spinner, indexOfChild);
				spinner.performClick();
				

			}
		});
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onClick(View v) {
		if(v == preferencesSaveButton){
			Preferences preferences = new Preferences();
			EditText currencySymbol = (EditText) findViewById(R.id.currencySymbol);
			String currency;
			if(currencySymbol == null || currencySymbol.getText() == null){
				currency = "";
			}else{
				currency = currencySymbol.getText().toString();
			}
			preferences.putCurrency(currency);
			Persister.getInstance(this).savePreferences(preferences);
			finish();
		}else if(v == preferencesCancelButton){
			finish();
		} 
		if(findViewById(R.id.currencySymbol) != null){
			KeyboardHider.hide(this,findViewById(R.id.currencySymbol));
		}
	}
}
