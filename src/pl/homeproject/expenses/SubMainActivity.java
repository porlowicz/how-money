package pl.homeproject.expenses;

import java.util.LinkedList;
import java.util.List;

import org.achartengine.GraphicalView;
import org.achartengine.renderer.DefaultRenderer;

import pl.homeproject.expenses.handler.graph.PieChartFactory;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.popupWindow.AbstractPopupWindow;
import pl.homeproject.expenses.popupWindow.ModifyBudgetNamePopupWindow;
import pl.homeproject.expenses.register.TileFactory;
import pl.homeproject.expenses.register.TilesViewManager;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class SubMainActivity extends MainActivity{

	private Category budget;

	@Override
	public int getBudgetTypeId() {
		return budget.getId();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		budget = (Category) getIntent().getSerializableExtra("budget");
		super.onCreate(savedInstanceState);
		updateActionBar();
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	public void updateActionBar() {
		getSupportActionBar().setTitle("Folder: " + budget.getCategory().toUpperCase());
	}
	
	@Override
	protected void showTiles() {
		
		TilesViewManager manager = new TilesViewManager(this);
		List<View> tiles = manager.getTilesFor(budget);
		
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		for (View tileView : tiles) {
			tilesGrid.addView(tileView);
		}
		addSettingsTile();
		addChartTile();
		
		if(android.os.Build.VERSION.SDK_INT < 11){
			findViewById(R.id.dndTilesGrid).requestLayout();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.submain, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.renameBudget:
			AbstractPopupWindow popupWindow = new ModifyBudgetNamePopupWindow(this, budget);
			popupWindow.show();
			return true;
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public Category getBudget() {
		return budget;
	}

	public void setBudget(Category budget) {
		this.budget = budget;
	}
	
	public void addChartTile() {
		ViewGroup chartTile = (ViewGroup) getLayoutInflater().inflate(
				R.layout.chart_tile_layout, null);

		TileFactory manager = new TileFactory(this);
		List<TileViewModel> tiles = manager.getTileViewsFor(budget);

		double sum = 0;
		List<TileViewModel> filteredTiles = new LinkedList<TileViewModel>();
		for (TileViewModel tileViewModel : tiles) {
			sum += tileViewModel.getDebit();
			if(tileViewModel.getDebit() != 0){
				filteredTiles.add(tileViewModel);
			}
		}

		GraphicalView pieChart = new PieChartFactory(this,
				new DefaultRenderer()).createPieChartThumbnail(filteredTiles, sum);
		if(pieChart == null){
			return;
		}
		chartTile.addView(pieChart);

		pieChart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent graphIntent = new Intent(SubMainActivity.this, GraphActivity.class);
				graphIntent.putExtra("budget", budget);
				SubMainActivity.this.startActivity(graphIntent);
			}
		});
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		tilesGrid.addView(chartTile);
	}
	
	public void replaceAddTileWithDelete(){

		View deleteTileLayout = getLayoutInflater().inflate(R.layout.back_and_delete_tile_layout, null);
		ViewGroup tilesGrid = (ViewGroup) findViewById(R.id.tilesGrid);
		View addRemoveSetting = findViewById(R.id.addRemoveSetting);
		tilesGrid.removeView(addRemoveSetting);
		tilesGrid.addView(deleteTileLayout);
		View showChartTile = tilesGrid.findViewById(R.id.showChartTile);
		tilesGrid.removeView(showChartTile);
		addChartTile();
	}
	
}
