package pl.homeproject.expenses;

import java.util.Calendar;
import java.util.List;

import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.WidgetConfigButton;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import pl.homeproject.expenses.view.helper.StringControl;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RemoteViews;


public class WidgetConfig extends Activity{
	
	public static String SHARED_PREFERENCES_NAME = "HowMoneyPreferences";
	private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_widget_config);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		LinearLayout widgetContent = (LinearLayout) findViewById(R.id.widgetConfigScrollViewContent);
		
		Persister persister = Persister.getInstance(this);
		
		List<Category> budgets = persister.findAllBudgets();
		for (Category category : budgets) {
			List<Category> categoriesForBudget = persister.findCategoriesFor(category);
			for (Category categoryEntry : categoriesForBudget) {
				View row = getLayoutInflater().inflate(R.layout.widget_config_row, null);
				final WidgetConfigButton rowButton = (WidgetConfigButton) row.findViewById(R.id.widgetConfigRowText);
				rowButton.setLabel(category.getCategory() + "/" + categoryEntry.getCategory());
				rowButton.setData(categoryEntry);
				rowButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setupWidget(rowButton);
					}
				});
				widgetContent.addView(row);
			}
		}
		List<Category> categories = persister.findStandaloneCategories();
		for (Category category : categories) {
			View row = getLayoutInflater().inflate(R.layout.widget_config_row, null);
			final WidgetConfigButton rowButton = (WidgetConfigButton) row.findViewById(R.id.widgetConfigRowText);
			rowButton.setLabel(category.getCategory());
			rowButton.setData(category);
			rowButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setupWidget(rowButton);
				}
			});
			widgetContent.addView(row);
		}
		
		
	}

	protected void setupWidget(WidgetConfigButton rowButton) {
		
		RemoteViews views = new RemoteViews(this.getPackageName(), R.layout.howmoney_widget);
		
		Persister persister = Persister.getInstance(this);
		
		Balance balance = persister.findBalanceFor(rowButton.getData(), Calendar.getInstance());
		
		int moneyLeft = balance.getLimit() - balance.getDebit();
		
		Category data = rowButton.getData();
		if(data.getParentCategory() != null){
			views.setTextViewText(R.id.widgetBudgetName, StringControl.cut(data.getParentCategory().getCategory(), 10) + ":");
			views.setTextViewText(R.id.widgetCategoryName, StringControl.cut(data.getCategory(), 10));
		}else{
			views.setViewVisibility(R.id.widgetCategoryName, View.INVISIBLE);
			views.setTextViewText(R.id.widgetBudgetName, StringControl.cut(data.getCategory(), 10));
		}
		views.setTextViewText(R.id.widgetMoneyLeft, DecimalFormatter.formatDecimalNumber(String.valueOf(moneyLeft)));
		
		Intent defineIntent = new Intent(this, DetailsActivity.class);
		defineIntent.putExtra("category", data);
		PendingIntent pendingIntent = PendingIntent.getActivity(
				this, mAppWidgetId, defineIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
		views.setOnClickPendingIntent(R.id.homescreenWidgetTopView, pendingIntent);
		// Tell the widget manager
		AppWidgetManager.getInstance(this).updateAppWidget(mAppWidgetId, views);
		
		Editor edit = getSharedPreferences(SHARED_PREFERENCES_NAME, 0).edit();
		edit.putInt(String.valueOf(mAppWidgetId), data.getId());
		edit.commit();
		
		Intent resultValue = new Intent(); 
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId); 
		setResult(RESULT_OK, resultValue); 
		finish();
	}
	
	
	public static void updateWidgets(Activity activity){
		Intent intent = new Intent(activity, HowMoneyWidgetProvider.class);
		intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
		int ids[] = AppWidgetManager.getInstance(activity.getApplication()).getAppWidgetIds(new ComponentName(activity.getApplication(), HowMoneyWidgetProvider.class));
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
		activity.sendBroadcast(intent);
	}
}


