package pl.homeproject.expenses.fragment;

import java.util.Calendar;

public interface DateHost {

	void setDate(Calendar date);
	Calendar getDate();
	
}
