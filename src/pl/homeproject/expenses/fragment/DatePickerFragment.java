package pl.homeproject.expenses.fragment;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

public abstract class DatePickerFragment extends DialogFragment implements OnDateSetListener {

	@SuppressLint("NewApi")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = getDateHost().getDate();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		
		DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
		datePickerDialog.setCancelable(true);
		if(android.os.Build.VERSION.SDK_INT >= 11){
			datePickerDialog.getDatePicker().setCalendarViewShown(true);
			datePickerDialog.getDatePicker().setSpinnersShown(false);
		}
		
		return datePickerDialog;
	}
	
	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar date = Calendar.getInstance();
		date.set(year, monthOfYear, dayOfMonth); 
		getDateHost().setDate(date);
	}

	abstract protected DateHost getDateHost();
}
