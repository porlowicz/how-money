package pl.homeproject.expenses.fragment;

import pl.homeproject.expenses.MainActivity;

public class MainActivityDatePickerFragment extends DatePickerFragment{

	@Override
	protected DateHost getDateHost() {
		return (MainActivity) getActivity();
	}
}
