package pl.homeproject.expenses.frontend.tiles.gesture;

import pl.homeproject.expenses.frontend.tiles.gesture.impl.TileDragEvent;
import android.view.View;

public interface DragAndDropStrategy {
	public boolean execute(View v, TileDragEvent event);
}
