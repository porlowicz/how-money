package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.frontend.tiles.gesture.DragAndDropStrategy;
import pl.homeproject.expenses.handler.DndScrollHelper;
import pl.homeproject.expenses.view.TileView;

public class DragEndStrategy implements DragAndDropStrategy{

	private DndScrollHelper dndScrollHelper;
	private MainActivity activity;

	public DragEndStrategy(MainActivity activity, DndScrollHelper dndScrollHelper) {
		this.activity = activity;
		this.dndScrollHelper = dndScrollHelper;
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean execute(View v, TileDragEvent event) {
		dndScrollHelper.stopScrolling();
		View localState = (View) event.getLocalState();
		localState.setVisibility(View.VISIBLE);
		if(localState instanceof TileView){
			((View)localState.getParent()).findViewById(R.id.budgetStateIndicator).setVisibility(View.VISIBLE);
		}
		
		View addTileTemplateView = activity.findViewById(R.id.addRemoveSetting);
		View showChartTile = activity.findViewById(R.id.showChartTile);
		ViewGroup tilesGrid = (ViewGroup) activity.findViewById(R.id.tilesGrid);
		tilesGrid.removeView(showChartTile);
		tilesGrid.removeView(addTileTemplateView);
		activity.addSettingsTile();
		activity.addChartTile();
		return true;
	}

}
