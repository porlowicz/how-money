package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import android.annotation.SuppressLint;
import android.view.DragEvent;
import android.view.View;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.frontend.tiles.gesture.DragAndDropStrategy;
import pl.homeproject.expenses.handler.DndScrollHelper;
import pl.homeproject.expenses.view.TileView;

public class DragExitStrategy implements DragAndDropStrategy{

	private View scrollUpHelper;
	private DndScrollHelper dndScrollHelper;
	private View scrollDownHelper;

	public DragExitStrategy(View scrollUpHelper, View scrollDownHelper, DndScrollHelper dndScrollHelper) {
		this.scrollUpHelper = scrollUpHelper;
		this.scrollDownHelper = scrollDownHelper;
		this.dndScrollHelper = dndScrollHelper;
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean execute(View v, TileDragEvent event) {
		if(v == scrollUpHelper){
			scrollUpHelper.setTranslationY(-25);
			dndScrollHelper.stopScrolling();
			return false;
		}else if(v == scrollDownHelper){
			scrollDownHelper.setTranslationY(25);
			dndScrollHelper.stopScrolling();
			return false;
		}else{
			if(v instanceof TileView && !((TileView) v).isBudget()){
				((View)v.getParent()).findViewById(R.id.dropAndCreateFolderFrameLayout).setVisibility(View.GONE);
				((View)v.getParent()).findViewById(R.id.budgetTileDetailsFrameLayout).setVisibility(View.VISIBLE);
				v.setBackground(v.getResources().getDrawable(R.drawable.page_with_logo));
			}
		}
		return true;
	}

}
