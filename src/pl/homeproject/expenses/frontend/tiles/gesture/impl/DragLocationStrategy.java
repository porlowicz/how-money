package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.frontend.tiles.gesture.DragAndDropStrategy;
import pl.homeproject.expenses.handler.DndScrollHelper;
import pl.homeproject.expenses.view.FrameLayoutAdapter;
import pl.homeproject.expenses.view.TileView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

public class DragLocationStrategy implements DragAndDropStrategy {

	private View scrollUpHelper;
	private View scrollDownHelper;
	private DndScrollHelper dndScrollHelper;
	private Activity activity;

	public DragLocationStrategy(Activity activity, View scrollUpHelper,
			View scrollDownHelper, DndScrollHelper dndScrollHelper) {
		this.activity = activity;
		this.scrollUpHelper = scrollUpHelper;
		this.scrollDownHelper = scrollDownHelper;
		this.dndScrollHelper = dndScrollHelper;
	}

	@SuppressLint("NewApi")
	public boolean execute(View v, TileDragEvent event) {
		// bad code, scrollers have to be first because setting tile is of type
		// !isBudget
		if (v == scrollUpHelper) {
			scrollUpHelper.setTranslationY(0);
			dndScrollHelper.setStep(-1);
			dndScrollHelper.startScrolling();
			return true;
		} else if (v == scrollDownHelper) {
			scrollDownHelper.setTranslationY(0);
			dndScrollHelper.setStep(2);
			dndScrollHelper.startScrolling();
			return true;
		}
		View draggedView = (View) event.getLocalState();
		TileView draggedTileTemplateView = (TileView) draggedView
				.findViewById(R.id.tileTemplateView);
		TileView targetTileTemplateView = (TileView) v
				.findViewById(R.id.tileTemplateView);
		
		if (targetTileTemplateView != null
				&& v != activity.findViewById(R.id.topLevelFrame)
				&& draggedTileTemplateView != null
				&& ((targetTileTemplateView.isBudget() && draggedTileTemplateView
						.isBudget()) || (!targetTileTemplateView.isBudget() && !draggedTileTemplateView
						.isBudget()))) {
			if (targetTileTemplateView.isBudget() || activity instanceof SubMainActivity) {
				swapTiles(draggedTileTemplateView, targetTileTemplateView);
				return true;
			}

			int targetIndex = getIndexOfView(targetTileTemplateView);
			int draggedIndex = getIndexOfView(draggedTileTemplateView);
			boolean dragEven = draggedIndex % 2 == 0;
			boolean targetSmallerThanDrag = targetIndex < draggedIndex;

			if (dragEven && targetSmallerThanDrag) {
				if (draggedIndex - targetIndex == 1) {
					if (event.getY() < targetTileTemplateView.getHeight() / 2
							&& event.getX() > targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else {
					if (event.getY() < targetTileTemplateView.getHeight() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				}
			} else if (!dragEven && targetSmallerThanDrag) {
				if (draggedIndex - targetIndex == 1) {
					if (event.getX() < targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else if (draggedIndex - targetIndex == 2) {
					if (event.getY() < targetTileTemplateView.getHeight() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else if (draggedIndex - targetIndex == 3) {
					if (event.getY() < targetTileTemplateView.getHeight() / 2
							&& event.getX() < targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				}
			} else if (dragEven && !targetSmallerThanDrag) {
				if (targetIndex - draggedIndex == 1) {
					if (event.getX() > targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else if (targetIndex - draggedIndex == 2) {
					if (event.getY() > targetTileTemplateView.getHeight() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else if (targetIndex - draggedIndex == 3) {
					if (event.getY() > targetTileTemplateView.getHeight() / 2
							&& event.getX() > targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				}
			} else if (!dragEven && !targetSmallerThanDrag) {
				if (targetIndex - draggedIndex == 1) {
					if (event.getY() > targetTileTemplateView.getHeight() / 2
							&& event.getX() < targetTileTemplateView.getWidth() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				} else {
					if (event.getY() > targetTileTemplateView.getHeight() / 2) {
						targetTileTemplateView.setBackground(v.getResources()
								.getDrawable(R.drawable.page_with_logo));
						swapTilesWithBackgroundChange(draggedTileTemplateView,
								targetTileTemplateView);
					}
				}
			}
		}
		return true;
	}

	private int getIndexOfView(TileView targetTileTemplateView) {
		View targetParent = (View) targetTileTemplateView.getParent();
		return ((ViewGroup) targetParent.getParent())
				.indexOfChild(targetParent);
	}

	private void swapTilesWithBackgroundChange(TileView draggedTileTemplateView,
			TileView targetTileTemplateView) {
		((View) targetTileTemplateView.getParent()).findViewById(
				R.id.budgetTileDetailsFrameLayout).setVisibility(View.VISIBLE);
		((View) targetTileTemplateView.getParent()).findViewById(
				R.id.dropAndCreateFolderFrameLayout).setVisibility(View.GONE);
		swapTiles(draggedTileTemplateView, targetTileTemplateView);
	}
	
	private void swapTiles(TileView draggedTileTemplateView,
			TileView targetTileTemplateView) {
		ViewGroup tilesGrid = (ViewGroup) activity.findViewById(R.id.tilesGrid);


		View draggedParent = (View) draggedTileTemplateView.getParent();
		while (!(draggedParent instanceof FrameLayoutAdapter)) {
			draggedParent = (View) draggedParent.getParent();
		}
		View targetParent = (View) targetTileTemplateView.getParent();
		while (!(targetParent instanceof FrameLayoutAdapter)) {
			targetParent = (View) targetParent.getParent();
		}
		int indexOfTarget = tilesGrid.indexOfChild(targetParent);
		int indexOfDragged = tilesGrid.indexOfChild(draggedParent);

		tilesGrid.removeView(draggedParent);
		tilesGrid.addView(draggedParent, indexOfTarget);
		tilesGrid.removeView(targetParent);
		tilesGrid.addView(targetParent, indexOfDragged);
	}

}
