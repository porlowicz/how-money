package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import java.util.ArrayList;
import java.util.List;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.frontend.tiles.gesture.DragAndDropStrategy;
import pl.homeproject.expenses.handler.DndScrollHelper;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.popupWindow.CreateFolderFromDropPopupWindow;
import pl.homeproject.expenses.view.BackDeleteTileFrameLayoutAdapter;
import pl.homeproject.expenses.view.CategoryLimitedLengthTextView;
import pl.homeproject.expenses.view.CategoryTextView;
import pl.homeproject.expenses.view.TileView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class DropStrategy implements DragAndDropStrategy {

	private List<View> dropTargets;
	private MainActivity activity;

	public DropStrategy(MainActivity activity, List<View> dropTargets) {
		this.activity = activity;
		this.dropTargets = dropTargets;
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean execute(View v, TileDragEvent event) {
		View draggedView = event.getLocalState();
		TileView tileTemplateView = (TileView) draggedView.findViewById(R.id.tileTemplateView);
		if(dropTargets.contains(v) && !tileTemplateView.isBudget()){
			Intent intent = event.getClipData().getItemAt(0).getIntent();
			Category category = (Category) intent.getSerializableExtra("category");
			Category budgetName = ((CategoryLimitedLengthTextView)v.findViewById(R.id.tileCategory)).getCategory();
			budgetClicked(category, budgetName);
		}else if(v.getId() == R.id.addRemoveSetting && !(v instanceof BackDeleteTileFrameLayoutAdapter)){
			removeTile(draggedView);
		}else if(v instanceof BackDeleteTileFrameLayoutAdapter){
			float x = event.getX();
			float y = event.getY();
			if(isBack((BackDeleteTileFrameLayoutAdapter) v, x, y)){
				moveFromFolder(draggedView);
			}else{
				removeTile(draggedView);
			}
		}else {
			
			if(((View) v.getParent()).findViewById(R.id.dropAndCreateFolderFrameLayout).getVisibility() == View.VISIBLE){
				new CreateFolderFromDropPopupWindow(activity, v, draggedView).show();
			}
		}
		updateOrdinalNumbers();
		return true;
	}
	
	private void updateOrdinalNumbers() {
		ViewGroup tilesGrid = (ViewGroup) activity.findViewById(R.id.tilesGrid);
		
		List<Category> budgets = new ArrayList<Category>();
		List<Category> categories = new ArrayList<Category>();
		for(int i = 0; i < tilesGrid.getChildCount(); i++){
			View frameLayoutAdapter = tilesGrid.getChildAt(i);
			TileView childAt = (TileView) frameLayoutAdapter.findViewById(R.id.tileTemplateView);
			CategoryLimitedLengthTextView tileCategory = (CategoryLimitedLengthTextView) frameLayoutAdapter.findViewById(R.id.tileCategory);
			if(childAt != null && childAt.isBudget()){
				budgets.add(new Category(tileCategory.getCategory().getId(), "", i));
			}else if(childAt != null){
				categories.add(new Category(tileCategory.getCategory().getId(), "", i - budgets.size()));
			}
		}
		Persister.getInstance(activity).updateBudgetsOrder(budgets);
		Persister.getInstance(activity).updateCategoriesOrder(categories);
	}
	
	private void moveFromFolder(View draggedView) {
		CategoryTextView tileCategory = (CategoryTextView) draggedView.findViewById(R.id.tileCategory);
		final Category category = tileCategory.getCategory();
		final Category parentCategory = category.getParentCategory();
		
		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					Category updatedCategory = new Category(category.getId(), category.getCategory(), new Category(1, "DEFAULT", 0), 0);
					
					Persister.getInstance(activity).updateCategory(updatedCategory);
					activity.updateTiles();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					activity.updateTiles();
					break;
				}
			}
		};

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);
		alertDialogBuilder
				.setMessage(String
						.format("Remove %s category from the folder %s?\nYou'll find it afterwards on the start screen.",
								category.getCategory(), parentCategory.getCategory()));
		alertDialogBuilder.setPositiveButton("Yes, remove it", clickListener)
				.setNegativeButton("No, leave it", clickListener);
		alertDialogBuilder.show();
		
	}

	private boolean isBack(BackDeleteTileFrameLayoutAdapter v, float x, float y) {
		int height = v.getHeight();
		
		if(x/(height-y) < 1.0){
			return true;
		}
		return false;
	}

	private void removeTile(final View view) {
		TextView tileCategory = (TextView) view
				.findViewById(R.id.tileCategory);

		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					activity.removeCategory(view);
					activity.updateTiles();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					break;
				}
			}
		};
		
		TileView tileTemplateView = (TileView) view.findViewById(R.id.tileTemplateView);
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		String folderOrCategory = "category";
		if(tileTemplateView != null){
			if(tileTemplateView.isBudget()){
				folderOrCategory = "folder";
			}
		}
		builder.setMessage(String.format("Delete the %s \"%s\"?",
				folderOrCategory, tileCategory.getText()));
		builder.setPositiveButton("Yes, delete it", clickListener)
				.setNegativeButton("No, leave it", clickListener).show();
	}

	private void budgetClicked(Category category, Category budget) {
		Persister persister = Persister.getInstance(activity);
		persister.updateCategoryWith(category, budget);
		activity.updateTiles();
	}
}
