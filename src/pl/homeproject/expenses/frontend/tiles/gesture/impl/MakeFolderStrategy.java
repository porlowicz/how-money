package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import java.util.ArrayList;
import java.util.List;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.frontend.tiles.gesture.DragAndDropStrategy;
import pl.homeproject.expenses.view.TileView;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.BounceInterpolator;

public class MakeFolderStrategy implements DragAndDropStrategy {
	private List<View> dropTargets = new ArrayList<View>();
	private Activity activity;

	public MakeFolderStrategy(Activity activity, List<View> dropTargets) {
		this.activity = activity;
		this.dropTargets = dropTargets;
	}

	SurfaceView surfaceView;

	@SuppressLint("NewApi")
	@Override
	public boolean execute(View v, TileDragEvent event) {
		if (activity instanceof SubMainActivity) {
			return false;
		}
		View draggedView = (View) event.getLocalState();
		TileView draggedTileTemplateView = (TileView) draggedView
				.findViewById(R.id.tileTemplateView);

		if (v instanceof TileView && !((TileView) v).isBudget() && !draggedTileTemplateView.isBudget()) {
			v.setBackground(v.getResources().getDrawable(
					R.drawable.folder_with_logo_dnd));
			((View) v.getParent()).findViewById(
					R.id.budgetTileDetailsFrameLayout).setVisibility(View.GONE);
			((View) v.getParent()).findViewById(
					R.id.dropAndCreateFolderFrameLayout).setVisibility(
					View.VISIBLE);
		}


		if (dropTargets.contains(v) && !draggedTileTemplateView.isBudget()) {
			ObjectAnimator anim = ObjectAnimator.ofFloat(
					(Object) v.getParent(), "rotation", -50, 50, 0);
			anim.setInterpolator(new BounceInterpolator());
			anim.setDuration(1 * 1000);
			anim.start();
		}

		return true;
	}

}
