package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import java.util.ArrayList;
import java.util.List;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.DndScrollHelper;
import android.annotation.SuppressLint;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.ScrollView;

@SuppressLint("NewApi")
public class OrganizeDragAndDropHandler implements OnDragListener{

	private List<View> dropTargets = new ArrayList<View>();
	
	private DragLocationStrategy dragLocationStrategy;
	private DragExitStrategy dragExitStrategy;
	private DropStrategy dropStrategy;
	private DragEndStrategy dragEndStrategy;
	private MakeFolderStrategy makeFolder;

	
	public OrganizeDragAndDropHandler(MainActivity activity) {
		
		View scrollUpHelper = activity.findViewById(R.id.scrollUpHelper);
		View scrollDownHelper = activity.findViewById(R.id.scrollDownHelper);
		DndScrollHelper dndScrollHelper = new DndScrollHelper((ScrollView) activity.findViewById(R.id.topLevelView));
		
		activity.findViewById(R.id.scrollUpHelper).setOnDragListener(this);
		activity.findViewById(R.id.scrollDownHelper).setOnDragListener(this);
		
		dragLocationStrategy = new DragLocationStrategy(activity, scrollUpHelper, scrollDownHelper, dndScrollHelper);
		dragExitStrategy = new DragExitStrategy(scrollUpHelper, scrollDownHelper, dndScrollHelper);
		dropStrategy = new DropStrategy(activity, dropTargets);
		dragEndStrategy = new DragEndStrategy(activity, dndScrollHelper);
		makeFolder = new MakeFolderStrategy(activity, dropTargets);
	}

	@Override
	public boolean onDrag(View v, DragEvent e) {
		
		TileDragEvent event = new TileDragEvent(e, v.getWidth(), v.getHeight());
		int action = e.getAction();
		switch (action) {
		case DragEvent.ACTION_DRAG_ENTERED:
			return makeFolder.execute(v, event);
		case DragEvent.ACTION_DRAG_EXITED:
			return dragExitStrategy.execute(v, event);
		case DragEvent.ACTION_DROP:
			return dropStrategy.execute(v, event);
		case DragEvent.ACTION_DRAG_LOCATION:
			return dragLocationStrategy.execute(v, event);
		case DragEvent.ACTION_DRAG_ENDED:
			return dragEndStrategy.execute(v, event);
		default:
			return true;
		}
	}

	public void addDropTarget(View tileTemplateView) {
		dropTargets.add(tileTemplateView);
	}

}
