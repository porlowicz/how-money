package pl.homeproject.expenses.frontend.tiles.gesture.impl;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.media.JetPlayer;
import android.view.DragEvent;
import android.view.View;

@SuppressLint("NewApi")
public class TileDragEvent {

	private final DragEvent event;
	private final float width;
	private final float height;
	
	public TileDragEvent(DragEvent event){
		this(event, 0, 0);
	}

	public TileDragEvent(DragEvent event, float x, float y){
		this.event = event;
		this.width = x;
		this.height = y;
	}
	
	public View getLocalState(){
		return (View) event.getLocalState();
	}
	
	public float getX(){
		return event.getX();
	}
	
	public float getY(){
		return event.getY();
	}
	
	public float getLeft(){
		return event.getX() - width/2;
	}

	public float getRight(){
		return event.getX() + width/2;
	}

	public float getTop(){
		return event.getY() - height/2;
	}

	public float getBottom(){
		return event.getX() + width/2;
	}

	public ClipData getClipData() {
		return event.getClipData();
	}
}
