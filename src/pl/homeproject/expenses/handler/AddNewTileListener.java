package pl.homeproject.expenses.handler;

import pl.homeproject.expenses.popupWindow.AbstractPopupWindow;
import pl.homeproject.expenses.popupWindow.AddNewTilePopupWindow;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class AddNewTileListener implements OnClickListener {

	private Context context;

	public AddNewTileListener(Context mainActivity) {
		this.context = mainActivity;
	}

	@Override
	public void onClick(View view) {
		AbstractPopupWindow addNewTilePopupWindow = new AddNewTilePopupWindow(context);
		addNewTilePopupWindow.show();
	}
}