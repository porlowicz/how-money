package pl.homeproject.expenses.handler;

import java.util.Calendar;

public interface DateChangedListener {

	void updateDate(Calendar date);
	
}
