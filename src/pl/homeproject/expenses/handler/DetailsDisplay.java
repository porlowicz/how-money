package pl.homeproject.expenses.handler;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class DetailsDisplay implements OnClickListener {

	private Context context;
	private Category category;

	public DetailsDisplay(Context context, Category category) {
		this.context = context;
		this.category = category;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(context, DetailsActivity.class);
		intent.putExtra("category", category);
		context.startActivity(intent);
	}

}
