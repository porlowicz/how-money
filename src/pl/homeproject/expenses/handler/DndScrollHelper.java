package pl.homeproject.expenses.handler;

import android.widget.ScrollView;

public class DndScrollHelper implements Runnable{

	private volatile boolean isScrolling = false;
	private float step = 20;
	private final ScrollView scrollView;
	
	public DndScrollHelper(ScrollView view) {
		scrollView = view;
	}
	
	public void startScrolling(){
		if(isScrolling){
			return;
		}
		isScrolling = true;
		Thread thread = new Thread(this);
		thread.start();
	}
	
	public void stopScrolling(){
		isScrolling = false;
	}

	synchronized public boolean isScrolling() {
		return isScrolling;
	}


	synchronized public void setScrolling(boolean isScrolling) {
		this.isScrolling = isScrolling;
	}


	public float getStep() {
		return step;
	}


	public void setStep(float step) {
		this.step = step * 35;
		if((int)this.step == 0){
			this.step = Math.signum(step);
		}
	}

	@Override
	public void run() {
		while(isScrolling()){
			scrollView.post(new Runnable() {
				
				@Override
				public void run() {
					scrollView.scrollBy(0, (int) step);					
				}
			});
			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
