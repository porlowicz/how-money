package pl.homeproject.expenses.handler;

import java.util.List;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.GraphActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.handler.graph.PieChartHolder;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.register.TileFactory;
import pl.homeproject.expenses.view.model.BudgetTileViewModel;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class GraphButtonsClickListener implements OnClickListener {

	private PieChartHolder pieChartHolder;
	private GraphActivity activity;

	public GraphButtonsClickListener(GraphActivity activity, PieChartHolder pieChartHolder) {
		this.activity = activity;
		this.pieChartHolder = pieChartHolder;
	}

	@Override
	public void onClick(View v) {
		TileViewModel highlightedTile = pieChartHolder.getHighlightedTile();
		Category category = highlightedTile.getCategory();
		switch (v.getId()) {
		case R.id.showGraphInnerFolderButton:
			activity.findViewById(R.id.graph).setVisibility(View.GONE);
			activity.findViewById(R.id.graphDetails).setVisibility(View.VISIBLE);
			
			TileFactory tileFactory = new TileFactory(activity);
			List<TileViewModel> tilesForBudget = tileFactory.getTileViewsFor(category);
			PieChartHolder pieChartHolder = new PieChartHolder(activity, tilesForBudget);
			View innerPieChart = pieChartHolder.createPieChartView(category.getCategory(), true);
			
			ViewGroup graphDetails = (ViewGroup) activity.findViewById(R.id.graphDetails);
			graphDetails.removeViewAt(0);
			graphDetails.addView(innerPieChart, 0);
			
			break;
		case R.id.showTimeseriesGraphButton:
			
			activity.setTimeseriesCategory(category);
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			
			break;
		case R.id.showDetailsButton:
			if(highlightedTile instanceof BudgetTileViewModel){
				Intent intent = new Intent(activity, SubMainActivity.class);
				intent.putExtra("budget", category);
				activity.startActivity(intent);
			}else if(highlightedTile instanceof TileViewModel){
				Intent intent = new Intent(activity, DetailsActivity.class);
				category = highlightedTile.getCategory();
				intent.putExtra("category", category);
				activity.startActivity(intent);				
			}
			break;
		}
	}
}
