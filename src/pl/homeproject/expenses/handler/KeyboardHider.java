package pl.homeproject.expenses.handler;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardHider {

//	public static void hide(Activity activity){
//	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//	    //Find the currently focused view, so we can grab the correct window token from it.
//	    View view = activity.getCurrentFocus();
//	    //If no view currently has focus, create a new one, just so we can grab a window token from it
//	    if(view == null) {
//	        view = new View(activity);
//	    }
//	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
//	}
	
	public static void hide(Context context, View focusRoot){
	    InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    View view = focusRoot.findFocus();
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	public static void show(Activity activity){
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.showSoftInput(view, 0);
	}
	
}
