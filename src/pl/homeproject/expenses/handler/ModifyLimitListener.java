package pl.homeproject.expenses.handler;

import java.util.Calendar;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.popupWindow.AbstractPopupWindow;
import pl.homeproject.expenses.popupWindow.ModifyLimitPopupWindow;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ModifyLimitListener implements OnClickListener{
	
		private Activity context;
		private Category category;
		private int limit;
		private Calendar date;
		private TextView limitLabel;

		public ModifyLimitListener(Activity activity, Category cat, int lim, Calendar calendar, TextView limitLabel) {
			context = activity;
			category = cat;
			limit = lim;
			date = calendar;
			this.limitLabel = limitLabel;
		}
		
		@Override
		public void onClick(View v) {
			AbstractPopupWindow limitPopupWindow = new ModifyLimitPopupWindow(context, limitLabel, category, limit, date);
			limitPopupWindow.show();
		}
	}
