package pl.homeproject.expenses.handler;

import java.util.Calendar;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.view.DetailView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RollExpensesHandler {

	public void toggleRoll(View v) {
		View detailTopRowAmountLayout = v
				.findViewById(R.id.detailTopRowAmountLayout);
		DetailView detailView = (DetailView) v;
		ViewGroup parent = (ViewGroup) v.getParent();
		int indexOfClickedView = parent.indexOfChild(v);

		if (detailTopRowAmountLayout.getVisibility() == View.VISIBLE) {
			rollUp(detailTopRowAmountLayout, detailView, parent,
					indexOfClickedView);
		} else {
			rollDown(detailTopRowAmountLayout, detailView, parent,
					indexOfClickedView);
		}
	}

	public void rollDown(View detailTopRowAmountLayout, DetailView detailView,
			ViewGroup parent, int indexOfClickedView) {
		DetailView loopDetailView = (DetailView) parent
				.getChildAt(indexOfClickedView + 1);
		if (indexOfClickedView + 1 >= parent.getChildCount()
				|| detailView.getDetailData().getDetailDate()
						.get(Calendar.DATE) != loopDetailView.getDetailData()
						.getDetailDate().get(Calendar.DATE)) {
			return;
		}
		int sumOfDebit = 0;
		for (int i = 1; indexOfClickedView + i < parent.getChildCount(); i++) {
			loopDetailView = (DetailView) parent.getChildAt(indexOfClickedView
					+ i);
			if (detailView.getDetailData().getDetailDate().get(Calendar.DATE) == loopDetailView
					.getDetailData().getDetailDate().get(Calendar.DATE)) {
				loopDetailView.setVisibility(View.GONE);
				sumOfDebit += loopDetailView.getDetailData().getAmount();
			} else {
				break;
			}
		}
		TextView detailTopRowAmount = (TextView) detailTopRowAmountLayout
				.findViewById(R.id.detailTopRowAmount);
		detailTopRowAmount.setText(String.valueOf(sumOfDebit));
		detailTopRowAmountLayout.setVisibility(View.VISIBLE);
		ImageView detailsTopRowImage = (ImageView) detailView
				.findViewById(R.id.detailsTopRowImage);
		detailsTopRowImage.setImageResource(R.drawable.up);
	}

	public void rollUp(View detailTopRowAmountLayout, DetailView detailView,
			ViewGroup parent, int indexOfClickedView) {
		if (indexOfClickedView + 1 == parent.getChildCount()
				|| ((ViewGroup) parent.getChildAt(indexOfClickedView + 1))
						.getChildAt(0).getId() == R.id.detailsTopRowGridLayout) {
			detailTopRowAmountLayout.setVisibility(View.VISIBLE);
		} else {
			detailTopRowAmountLayout.setVisibility(View.INVISIBLE);
		}
		for (int i = 1; indexOfClickedView + i < parent.getChildCount(); i++) {
			DetailView loopDetailView = (DetailView) parent
					.getChildAt(indexOfClickedView + i);
			if (detailView.getDetailData().getDetailDate().get(Calendar.DATE) == loopDetailView
					.getDetailData().getDetailDate().get(Calendar.DATE)) {
				loopDetailView.setVisibility(View.VISIBLE);
			} else {
				break;
			}
		}
		ImageView detailsTopRowImage = (ImageView) detailView
				.findViewById(R.id.detailsTopRowImage);
		detailsTopRowImage.setImageResource(R.drawable.down);
	}

}
