package pl.homeproject.expenses.handler;

import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class SubMainDisplay implements OnClickListener {

	private Context context;
	private Category budget;

	public SubMainDisplay(Context context, Category budget) {
		this.context = context;
		this.budget = budget;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(context, SubMainActivity.class);
		intent.putExtra("budget", budget);
		context.startActivity(intent);
	}

}
