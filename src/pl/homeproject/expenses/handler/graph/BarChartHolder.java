package pl.homeproject.expenses.handler.graph;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.register.data.DateFormat;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.view.View;

public class BarChartHolder {

	private Activity activity;
	private XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
	private Category category;
	private int year;
	private XYMultipleSeriesRenderer multipleSeriesRenderer = new XYMultipleSeriesRenderer();

	public BarChartHolder(Activity activity, Category category, int year) {
		this.activity = activity;
		this.category = category;
		this.year = year;
		
		multipleSeriesRenderer.setChartTitle(category.getCategory());
		multipleSeriesRenderer.setApplyBackgroundColor(true);
		multipleSeriesRenderer.setBackgroundColor(Color.argb(0, 0, 0, 0));
		multipleSeriesRenderer.setAxisTitleTextSize(activity.getResources().getDimension(R.dimen.chart_legend_textsize));
		multipleSeriesRenderer.setChartTitleTextSize(activity.getResources().getDimension(R.dimen.chart_title_textsize_small));
		multipleSeriesRenderer.setLabelsTextSize(activity.getResources().getDimension(R.dimen.chart_legend_textsize_small));
		multipleSeriesRenderer.setLegendTextSize(activity.getResources().getDimension(R.dimen.chart_legend_textsize));
		multipleSeriesRenderer.setYLabelsAlign(Align.RIGHT);
	}
	
	public GraphicalView createXYChart(){
		
		Calendar date = Calendar.getInstance();
		date.set(Calendar.MILLISECOND, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.HOUR, 0);
		date.set(Calendar.DATE, 1);
		date.set(Calendar.MONTH, 0);
		date.set(Calendar.YEAR, year);
		
		List<Balance> balanceData = new LinkedList<Balance>();
		Calendar iterDate = Calendar.getInstance();
		iterDate.setTime(date.getTime());
		for(int i = 0; i < 12; i++){
			Balance balance = Persister.getInstance(activity).findBalanceFor(category, iterDate);
			balanceData.add(balance);
			iterDate.add(Calendar.MONTH, 1);
		}
		
		Calendar debitDate = Calendar.getInstance();
		debitDate.setTime(date.getTime());
		TimeSeries series = new TimeSeries(activity.getResources().getString(R.string.static_text_expenses));
		int max = 0;
		for (Balance balance : balanceData) {
			series.add(debitDate.get(Calendar.MONTH), balance.getDebit()/100);
			debitDate.add(Calendar.MONTH, 1);
			if(balance.getDebit()/100 > max){
				max = balance.getDebit()/100;
			}
		}
		
		Calendar limitDate = Calendar.getInstance();
		limitDate.setTime(date.getTime());
		TimeSeries limitSeries = new TimeSeries(activity.getResources().getString(R.string.static_text_limit));
		for (Balance balance : balanceData) {
			limitSeries.add(limitDate.get(Calendar.MONTH), balance.getLimit()/100);
			limitDate.add(Calendar.MONTH, 1);
			if(balance.getLimit()/100 > max){
				max = balance.getLimit()/100;
			}
		}
		
		XYSeriesRenderer debitRenderer = new XYSeriesRenderer();
		debitRenderer.setLineWidth(5f);
		debitRenderer.setColor(Color.RED);
		multipleSeriesRenderer.setMargins(new int[]{50,100,(int)activity.getResources().getDimension(R.dimen.chart_margin_bottom),100});
		multipleSeriesRenderer.setXAxisMin(-1);
		multipleSeriesRenderer.setXAxisMax(12);
		multipleSeriesRenderer.setYAxisMax(max*1.1);
		multipleSeriesRenderer.setLegendHeight((int) activity.getResources().getDimension(R.dimen.chart_legend_textsize));

		XYSeriesRenderer limitRenderer = new XYSeriesRenderer();
		limitRenderer.setLineWidth(5f);
		limitRenderer.setColor(Color.GREEN);
		
		Calendar monthHelper = Calendar.getInstance();
		monthHelper.set(Calendar.MONTH, 0);
		for(int i = 0; i < 12; i++){
			multipleSeriesRenderer.addXTextLabel(i, DateFormat.formatChartMonth(monthHelper.getTime()));
			monthHelper.add(Calendar.MONTH, 1);
		}
		multipleSeriesRenderer.setXLabels(0);
		multipleSeriesRenderer.setYLabels(15);
		multipleSeriesRenderer.setShowGridY(true);
		multipleSeriesRenderer.setShowGridX(true);
		
		multipleSeriesRenderer.setBarSpacing(0.5);

		multipleSeriesRenderer.setPanEnabled(false);
		multipleSeriesRenderer.setZoomEnabled(false);
		multipleSeriesRenderer.setZoomEnabled(false, false);
		multipleSeriesRenderer.setExternalZoomEnabled(false);
		
		dataset.addSeries(limitSeries);
		dataset.addSeries(series);
		
		multipleSeriesRenderer.addSeriesRenderer(limitRenderer);
		multipleSeriesRenderer.addSeriesRenderer(debitRenderer);
		
		GraphicalView lineChartView = ChartFactory.getBarChartView(activity, dataset, multipleSeriesRenderer, Type.DEFAULT);
		lineChartView.setBackgroundColor(Color.BLACK);
		lineChartView.repaint();
		hideBudgetButtons();
		
		return lineChartView;
	}
	
	private void hideBudgetButtons(){
		activity.findViewById(R.id.graphsMenuButtons).setVisibility(View.GONE);
	}
	
}
