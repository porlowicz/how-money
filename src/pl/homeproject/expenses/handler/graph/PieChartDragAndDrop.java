package pl.homeproject.expenses.handler.graph;

import java.util.List;

import org.achartengine.GraphicalView;
import org.achartengine.chart.PieChart;
import org.achartengine.model.Point;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import pl.homeproject.expenses.Debugger;
import pl.homeproject.expenses.GraphActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.GraphButtonsClickListener;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import pl.homeproject.expenses.view.helper.StringControl;
import pl.homeproject.expenses.view.model.BudgetTileViewModel;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.annotation.SuppressLint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("NewApi")
public class PieChartDragAndDrop implements OnDragListener{
	
	private PieChartHolder holder;

	public PieChartDragAndDrop(PieChartHolder holder) {
		this.holder = holder;
		
	}
	
	@Override
	public boolean onDrag(View v, DragEvent event) {
		switch (event.getAction()) {
		case DragEvent.ACTION_DRAG_STARTED:
			if(holder.getHighlightedTile() != null){
				holder.showButtons();
			}
			break;
		case DragEvent.ACTION_DRAG_LOCATION:
			PieChart chart = (PieChart) holder.getPieChartView().getChart();
			float centerX = chart.getCenterX();
			float centerY = chart.getCenterY();

			float vecX = event.getX() - centerX;
			float vecY = event.getY() - centerY;

			SeriesSelection selection = chart
					.getSeriesAndPointForScreenCoordinate(new Point(event
							.getX() - vecX * 0.9f, event.getY() - vecY * 0.9f));
			if (selection != null) {
				holder.highlightPie(selection);
				holder.getPieChartView().repaint();
			}
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			holder.showButtons();
			break;
		}

		return true;
	}
	
}
