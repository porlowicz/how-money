package pl.homeproject.expenses.handler.graph;

import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.view.helper.StringControl;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.app.Activity;
import android.graphics.Color;

public class PieChartFactory {

	private DefaultRenderer renderer;
	private Activity activity;
	
	public PieChartFactory(Activity act, DefaultRenderer rend) {
		activity = act;
		renderer = rend;
	}
	
	public GraphicalView createPieChart(String title, List<TileViewModel> tiles, double sum){
		if(sum == 0){
			return null;
		}

		CategorySeries dataset = new CategorySeries(title);
		buildData(tiles, sum, dataset);
		
		renderer.setChartTitleTextSize(activity.getResources().getDimension(R.dimen.chart_title_textsize_small));
		renderer.setChartTitle(StringControl.cut(title, 25));
		renderer.setFitLegend(true);
		renderer.setLabelsTextSize(activity.getResources().getDimension(R.dimen.chart_legend_textsize));
		renderer.setZoomEnabled(false);
		renderer.setPanEnabled(false);
		renderer.setScale(0.7f);
		renderer.setShowLegend(false);
		renderer.setClickEnabled(true);
		renderer.setInScroll(true);
		
		return ChartFactory.getPieChartView(activity, dataset, renderer);
	}

	private void buildData(List<TileViewModel> tiles, double sum,
			CategorySeries dataset) {
		int i = 0;
		for (TileViewModel tile : tiles) {
			String category = tile.getCategory().getCategory();
			dataset.add(category, tile.getDebit()/sum);
			SimpleSeriesRenderer simpleRenderer = new SimpleSeriesRenderer();
			simpleRenderer.setColor(getColor(i, tiles.size()));
			renderer.addSeriesRenderer(simpleRenderer);
			i++;
		}
		
	}
	
	public GraphicalView createPieChartThumbnail(List<TileViewModel> tiles, double sum){
		if(sum == 0){
			return null;
		}

		CategorySeries dataset = new CategorySeries("Pie chart");
		buildData(tiles, sum, dataset);
		
		renderer.setFitLegend(false);
		renderer.setZoomEnabled(false);
		renderer.setExternalZoomEnabled(false);
		renderer.setPanEnabled(false);
		renderer.setShowLabels(false);
		renderer.setShowLegend(false);
		renderer.setClickEnabled(false);
		renderer.setInScroll(true);
		
		return ChartFactory.getPieChartView(activity, dataset, renderer);
	}
	
	
	private int getColor(int i, int numberOfColors){
		float nextColorIndex = (i+1) * 360/(float)(numberOfColors+2);
	    return Color.HSVToColor(new float[]{nextColorIndex, 0.84f, 1f});
	}

}
