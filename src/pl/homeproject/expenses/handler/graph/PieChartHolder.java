package pl.homeproject.expenses.handler.graph;

import java.util.List;

import org.achartengine.GraphicalView;
import org.achartengine.chart.PieChart;
import org.achartengine.model.Point;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import pl.homeproject.expenses.Debugger;
import pl.homeproject.expenses.GraphActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.GraphButtonsClickListener;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import pl.homeproject.expenses.view.helper.StringControl;
import pl.homeproject.expenses.view.model.BudgetTileViewModel;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.graphics.PorterDuff.Mode;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class PieChartHolder{
	
	private final DefaultRenderer renderer = new DefaultRenderer();;
	private GraphActivity activity;
	private List<TileViewModel> tiles;
	private GraphicalView pieChartView;
	private TileViewModel highlightedTile;
	private GraphButtonsClickListener buttonsClickListener;
	
	public PieChartHolder(GraphActivity activity, List<TileViewModel> tiles) {
		this.activity = activity;
		this.tiles = tiles;
	}
	
	@SuppressLint("NewApi")
	public View createPieChartView(String title, boolean isBudget) {
		activity.findViewById(R.id.noDataToBeDisplayed).setVisibility(View.GONE);
		
		double sum = 0;
		for (int i = 0; i < tiles.size(); ) {
			if(tiles.get(i).getDebit() == 0){
				tiles.remove(i);
			}else{
				sum += tiles.get(i).getDebit();
				i++;
			}
		}
		
		pieChartView = new PieChartFactory(activity, renderer).createPieChart(title, tiles, sum);
		
		if (pieChartView == null) {
			activity.findViewById(R.id.noDataToBeDisplayed).setVisibility(View.VISIBLE);
			return new View(activity);
		}

		pieChartView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				SeriesSelection s = pieChartView.getChart().getSeriesAndPointForScreenCoordinate(new Point(event.getX(), event.getY()));
				return false;
			}
		});
		
		pieChartView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hideBudgetButtons();
				SeriesSelection currentSeriesAndPoint = pieChartView.getCurrentSeriesAndPoint();
				if(currentSeriesAndPoint != null){
					highlightPie(currentSeriesAndPoint);
					showButtons();
				}
			}

		});
		
		pieChartView.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if(android.os.Build.VERSION.SDK_INT >= 11){
					pieChartView.startDrag(null, new View.DragShadowBuilder(), pieChartView, 0);
				}else{
					// oldApi support
				}
				return false;
			}
		});

		if(android.os.Build.VERSION.SDK_INT >= 11){
			pieChartView.setOnDragListener(new PieChartDragAndDrop(this));
		}else{
			// oldApi support
		}
		hideBudgetButtons();
		
		ViewGroup graphLegend;
		if(isBudget){
			graphLegend = (ViewGroup) activity.findViewById(R.id.graphDetailsLegend);
		}else{
			graphLegend = (ViewGroup) activity.findViewById(R.id.graphLegend);
		}
		graphLegend.removeAllViews();
		
		int i = 0;
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		String currenctSymbol = Preferences.getCurrencySymbol(activity);
		for (TileViewModel tile : tiles) {
			if(tile.getDebit() == 0){
				continue;
			}

			View row = layoutInflater.inflate(R.layout.row_legend, null);
			
			TextView legendRowTitle = (TextView) row.findViewById(R.id.legendRowTitle);
			TextView legendRowAmout = (TextView) row.findViewById(R.id.legendRowAmount);
			
			SimpleSeriesRenderer seriesRendererAt = renderer.getSeriesRendererAt(i);
			legendRowTitle.setTextColor(seriesRendererAt.getColor());
			legendRowAmout.setTextColor(seriesRendererAt.getColor());
			String category = tile.getCategory().getCategory();
			legendRowAmout.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			legendRowTitle.setText(StringControl.cut(category, 15));
			legendRowAmout.setText(String.format("%s%s", DecimalFormatter.formatDecimalNumber(String.valueOf(tile.getDebit())), currenctSymbol));
			graphLegend.addView(row);
			i++;
		}
		renderer.setLabelsColor(Color.WHITE);
		
		buttonsClickListener = new GraphButtonsClickListener(activity, this);
		setOnClickListeners();
		return pieChartView;
	}

	public void setOnClickListeners() {
		activity.findViewById(R.id.showGraphInnerFolderButton).setOnClickListener(buttonsClickListener);
		activity.findViewById(R.id.showTimeseriesGraphButton).setOnClickListener(buttonsClickListener);
		activity.findViewById(R.id.showDetailsButton).setOnClickListener(buttonsClickListener);
	}
	
	private void showTileButtons() {
		activity.findViewById(R.id.showGraphInnerFolderButton).setVisibility(View.GONE);
		activity.findViewById(R.id.showTimeseriesGraphButton).setVisibility(View.VISIBLE);
		activity.findViewById(R.id.showDetailsButton).setVisibility(View.VISIBLE);
		Button button = (Button) activity.findViewById(R.id.showDetailsButton);
		button.setText(R.string.show_details_expenses);
	}
	private void showBudgetButtons() {
		activity.findViewById(R.id.showGraphInnerFolderButton).setVisibility(View.VISIBLE);
		Button button = (Button) activity.findViewById(R.id.showGraphInnerFolderButton);
		button.setText(String.format(activity.getString(R.string.show_inner_budgets), StringControl.cut(getHighlightedTile().getCategory().getCategory(), 30)));
		activity.findViewById(R.id.showTimeseriesGraphButton).setVisibility(View.GONE);
		activity.findViewById(R.id.showDetailsButton).setVisibility(View.VISIBLE);
		button = (Button) activity.findViewById(R.id.showDetailsButton);
		button.setText(R.string.show_details);
	}
	private void hideBudgetButtons(){
		activity.findViewById(R.id.selectedPieHint).setVisibility(View.GONE);
		activity.findViewById(R.id.graphLegend).setVisibility(View.VISIBLE);
		activity.findViewById(R.id.graphDetailsLegend).setVisibility(View.VISIBLE);
		activity.findViewById(R.id.graphsMenuButtons).setVisibility(View.GONE);
	}
	void showButtons() {
		activity.findViewById(R.id.selectedPieHint).setVisibility(View.VISIBLE);
		activity.findViewById(R.id.graphLegend).setVisibility(View.GONE);
		activity.findViewById(R.id.graphDetailsLegend).setVisibility(View.GONE);
		if(highlightedTile instanceof BudgetTileViewModel){
			showBudgetButtons();
		}else if(highlightedTile instanceof TileViewModel){
			showTileButtons();
		}
		activity.findViewById(R.id.graphsMenuButtons).setVisibility(View.VISIBLE);
	}
	public TileViewModel getHighlightedTile(){
		return highlightedTile;
	}

	public DefaultRenderer getRenderer() {
		return renderer;
	}
	
	
	void highlightPie(SeriesSelection currentSeriesAndPoint) {
		highlightedTile = tiles.get(currentSeriesAndPoint.getPointIndex());
		for (int i = 0; i < renderer.getSeriesRendererCount(); i++) {
		    renderer.getSeriesRendererAt(i).setHighlighted(i == currentSeriesAndPoint.getPointIndex());
			if(i == currentSeriesAndPoint.getPointIndex()){
				int color = renderer.getSeriesRendererAt(i).getColor();
				TextView selectedPieHint = (TextView) activity.findViewById(R.id.selectedPieHint);
				selectedPieHint.setText(StringControl.cut(highlightedTile.getCategory().getCategory(), 55));
				Drawable border_rounded_corners = activity.getResources().getDrawable(R.drawable.border_rounded_corners);
				color &= 0x00ffffff;
				border_rounded_corners.setColorFilter(0x99000000 | color, Mode.MULTIPLY);
				selectedPieHint.setBackgroundDrawable(border_rounded_corners);
			}
		  }
		  pieChartView.repaint();
	}

	public GraphicalView getPieChartView() {
		return pieChartView;
	}

}
