package pl.homeproject.expenses.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.BudgetCategory;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.register.data.DetailData;
import pl.homeproject.expenses.register.data.ExpenseData;
import pl.homeproject.expenses.register.data.Period;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Persister extends SQLiteOpenHelper {

	private static final String DB_NAME = "db_expenses";
	private static final int DB_VERSION = 3;

	private static final String PREFERENCES = "preferences";
	private static final String PRE_ID = "id";
	private static final String PRE_KEY = "key";
	private static final String PRE_VALUE = "value";

	private static final String EXPENSES = "expenses";
	private static final String EXP_ID = "id";
	private static final String EXP_DATE = "date";
	private static final String EXP_AMOUNT = "amount";
	private static final String EXP_CATEGORY = "category";
	private static final String EXP_PERIOD_ID = "period_id";
	private static final String EXP_COMMENT = "comment";

	private static final String CATEGORIES = "categories";
	private static final String CAT_ID = "id";
	private static final String CAT_CATEGORY = "category";
	private static final String CAT_PARENT_CAT = "parent_category";
	private static final String CAT_TILE_ORDINAL_NO = "ordinal_number";

	private static final String PERIODS = "periods";
	private static final String PER_ID = "id";
	private static final String PER_FROM = "period_from";
	private static final String PER_TO = "period_to";

	private static final String LIMITS = "limits";
	private static final String LIM_PERIOD_ID = "period_id";
	private static final String LIM_LIMIT = "period_limit";
	private static final String LIM_CATEGORY = "category";

	private static final String BUDGET_TYPES = "budget_types";
	private static final String BUD_ID = "id";
	private static final String BUD_TYPE = "budget_type";
	private static final String BUD_TILE_ORDINAL_NO = "ordinal_number";

	private static final String PREFERENCES_CREATE = "CREATE TABLE "
			+ PREFERENCES + " (" + PRE_ID + " INTEGER PRIMARY KEY, " + PRE_KEY
			+ " TEXT NOT NULL UNIQUE," + PRE_VALUE + " TEXT NOT NULL);";

	private static final int NO_PARENT = 1;

	private static final String CATEGORIES_CREATE = "CREATE TABLE "
			+ CATEGORIES + " (" + CAT_ID + " INTEGER PRIMARY KEY, "
			+ CAT_CATEGORY + " TEXT NOT NULL," + CAT_TILE_ORDINAL_NO
			+ " INTEGER DEFAULT 0," + CAT_PARENT_CAT + " INTEGER DEFAULT "
			+ NO_PARENT + ", " + "FOREIGN KEY( " + CAT_PARENT_CAT
			+ ") REFERENCES " + BUDGET_TYPES + "(" + BUD_ID
			+ ") ON DELETE CASCADE);";

	private static final String PERIODS_CREATE = "CREATE TABLE " + PERIODS
			+ " (" + PER_ID + " INTEGER PRIMARY KEY, " + PER_FROM
			+ " INTEGER NOT NULL, " + PER_TO + " INTEGER NOT NULL);";

	private static final String EXPENSES_CREATE = "CREATE TABLE " + EXPENSES
			+ " (" + EXP_ID + " INTEGER PRIMARY KEY, " + EXP_DATE
			+ " INTEGER NOT NULL, " + EXP_AMOUNT + " INTEGER NOT NULL, "
			+ EXP_CATEGORY + " INTEGER NOT NULL, " + EXP_PERIOD_ID
			+ " INTEGER NOT NULL, " + EXP_COMMENT + " TEXT DEFAULT '', "
			+ "FOREIGN KEY(" + EXP_CATEGORY + ") REFERENCES " + CATEGORIES
			+ "(" + CAT_ID + ") ON DELETE CASCADE, " + "FOREIGN KEY("
			+ EXP_PERIOD_ID + ") REFERENCES " + PERIODS + "(" + PER_ID + "));";

	private static final String ALTER_EXPENSES_ADD_COMMENT_COLUMN = "ALTER TABLE "
			+ EXPENSES + " ADD COLUMN " + EXP_COMMENT + " TEXT DEFAULT '';";

	private static final String BUDGET_TYPES_CREATE = "CREATE TABLE "
			+ BUDGET_TYPES + " (" + BUD_ID + " INTEGER PRIMARY KEY, "
			+ BUD_TILE_ORDINAL_NO + " INTEGER DEFAULT 0, " + BUD_TYPE
			+ " TEXT NOT NULL);";

	private static final String LIMITS_CREATE = "CREATE TABLE " + LIMITS + " ("
			+ LIM_PERIOD_ID + " INTEGER NOT NULL, " + LIM_LIMIT
			+ " INTEGER NOT NULL, " + LIM_CATEGORY + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + LIM_CATEGORY + ") REFERENCES " + CATEGORIES
			+ "(" + CAT_ID + ") ON DELETE CASCADE," + "FOREIGN KEY("
			+ LIM_PERIOD_ID + ") REFERENCES " + PERIODS + "(" + PER_ID + "),"
			+ "UNIQUE (" + LIM_PERIOD_ID + ", " + LIM_CATEGORY + "));";

	private static final String ALTER_LIMIT_TABLE_DROP_BUDGET_COLUMN = "ALTER TABLE "
			+ LIMITS + " RENAME TO BAK_" + LIMITS + ";";

	private static final String COPY_DATA_TO_NEW_LIMITS_TABLE = "INSERT INTO "
			+ LIMITS + " (" + LIM_CATEGORY + ", " + LIM_LIMIT + ", "
			+ LIM_PERIOD_ID + ") SELECT " + LIM_CATEGORY + ", " + LIM_LIMIT
			+ ", " + LIM_PERIOD_ID + " FROM BAK_" + LIMITS + ";";

	private static final String DROP_BAK_LIMITS = "DROP TABLE IF EXISTS BAK_"
			+ LIMITS + ";";

	private static final String LIMITS_DROP = "DROP TABLE IF EXISTS " + LIMITS
			+ ";";
	private static final String EXPENSES_DROP = "DROP TABLE IF EXISTS "
			+ EXPENSES + ";";
	private static final String CATEGORIES_DROP = "DROP TABLE IF EXISTS "
			+ CATEGORIES + ";";
	private static final String PERIODS_DROP = "DROP TABLE IF EXISTS "
			+ PERIODS + ";";
	private static final String BUDGET_TYPES_DROP = "DROP TABLE IF EXISTS "
			+ BUDGET_TYPES + ";";
	private static final String PREFERENCES_DROP = "DROP TABLE IF EXISTS "
			+ PREFERENCES + ";";

	private static final String INSERT_EXPENSES = "INSERT INTO " + EXPENSES
			+ " (" + EXP_DATE + ", " + EXP_AMOUNT + ", " + EXP_CATEGORY + ", "
			+ EXP_PERIOD_ID + ", " + EXP_COMMENT
			+ ") VALUES (%s, %s, %s, %s, '%s');";

	private static final String INSERT_PREDEFINED_CATEGORIES = "INSERT INTO "
			+ CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
			+ "( 'Household' ), " + "( 'Car' ), " + "( 'Food' ), "
			+ "( 'Entertainment')," + "( 'Bills&Invoices'); ";

	private static final String[] INSERT_PREDEFINED_CATEGORIES_ARR = new String[] {
			"INSERT INTO " + CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
					+ "( 'Household' ); ",
			"INSERT INTO " + CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
					+ "( 'Car' ); ",
			"INSERT INTO " + CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
					+ "( 'Food' ); ",
			"INSERT INTO " + CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
					+ "( 'Entertainment'); ",
			"INSERT INTO " + CATEGORIES + "(" + CAT_CATEGORY + ") VALUES "
					+ "( 'Bills&Invoices'); " };

	private static final String INSERT_DEFAULT_BUDGET = "INSERT INTO "
			+ BUDGET_TYPES + "(" + BUD_TYPE + ") VALUES " + "('DEFAULT');";

	private static Persister persister;

	public static Persister getInstance(Context context) {
		if (persister == null) {
			persister = new Persister(context);
		}
		return persister;
	}

	private Persister(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		getWritableDatabase().close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(PREFERENCES_DROP);
		db.execSQL(LIMITS_DROP);
		db.execSQL(EXPENSES_DROP);
		db.execSQL(CATEGORIES_DROP);
		db.execSQL(PERIODS_DROP);
		db.execSQL(BUDGET_TYPES_DROP);
		db.execSQL(CATEGORIES_CREATE);
		db.execSQL(PERIODS_CREATE);
		db.execSQL(BUDGET_TYPES_CREATE);
		db.execSQL(LIMITS_CREATE);
		db.execSQL(EXPENSES_CREATE);
		db.execSQL(PREFERENCES_CREATE);

		for (String predefinedCategory : INSERT_PREDEFINED_CATEGORIES_ARR) {
			db.execSQL(predefinedCategory);
		}
		db.execSQL(INSERT_DEFAULT_BUDGET);
	}

	@Override
	public SQLiteDatabase getWritableDatabase() {
		SQLiteDatabase writableDatabase = super.getWritableDatabase();
		writableDatabase.execSQL("PRAGMA foreign_keys = ON;");
		return writableDatabase;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int old, int newV) {
		if (newV == 2) {
			db.execSQL(ALTER_LIMIT_TABLE_DROP_BUDGET_COLUMN);
			db.execSQL(LIMITS_CREATE);
			db.execSQL(COPY_DATA_TO_NEW_LIMITS_TABLE);
			db.execSQL(DROP_BAK_LIMITS);
		} else if (newV == 3) {
			db.execSQL(ALTER_EXPENSES_ADD_COMMENT_COLUMN);
		}
	}

	public void save(ExpenseData expense) {
		SQLiteDatabase db = getWritableDatabase();

		int categoryId = expense.getCategory().getId();
		int periodId = findPeriodFor(expense.getDate());

		if (categoryId < 0) {
			throw new CategoryIdIsMissing(String.format("No id for %s",
					expense.getCategory()));
		}
		String format = String.format(INSERT_EXPENSES, expense.getDate()
				.getTimeInMillis() / 1000, expense.getAmount(), categoryId,
				periodId, expense.getComment());
		db.execSQL(format);
	}

	private int findCategoryId(String category) {
		SQLiteDatabase db = getReadableDatabase();

		Cursor cursor = db.query(CATEGORIES, new String[] { CAT_ID },
				CAT_CATEGORY + " = ?", new String[] { category }, null, null,
				null);
		int catId = -1;
		if (cursor.moveToFirst()) {
			catId = cursor.getInt(cursor.getColumnIndex(CAT_ID));
		}
		cursor.close();
		return catId;
	}

	public List<ExpenseData> findAllDataFor(Period period) {
		return null;
	}

	public List<Category> findAllCategories() {

		List<Category> result = new ArrayList<Category>();

		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db
				.query(CATEGORIES, null, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Category cat = mapCategory(cursor);
				result.add(cat);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	public List<Category> findStandaloneCategories() {

		List<Category> result = new ArrayList<Category>();

		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(CATEGORIES, null, CAT_PARENT_CAT + " = ?",
				new String[] { "1" }, null, null, CAT_TILE_ORDINAL_NO);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Category cat = mapCategory(cursor);
				result.add(cat);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	private Category mapCategory(Cursor cursor) {
		int id = cursor.getInt(cursor.getColumnIndex(CAT_ID));
		String category = cursor.getString(cursor.getColumnIndex(CAT_CATEGORY));
		int ordinal = cursor.getInt(cursor.getColumnIndex(CAT_TILE_ORDINAL_NO));
		Category cat = new Category(id, category, ordinal);
		return cat;
	}

	private Category mapCategory(Cursor cursor, Category parentCategory) {
		int id = cursor.getInt(cursor.getColumnIndex(CAT_ID));
		String category = cursor.getString(cursor.getColumnIndex(CAT_CATEGORY));
		int ordinal = cursor.getInt(cursor.getColumnIndex(CAT_TILE_ORDINAL_NO));
		Category cat = new Category(id, category, parentCategory, ordinal);
		return cat;
	}

	public List<Category> findCategoriesFor(Category budget) {

		int budgetId = budget.getId();

		List<Category> result = new ArrayList<Category>();

		Category parentCategory = new Category(budgetId, budget.getCategory(),
				budget.getOrdinalNumber());

		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(CATEGORIES, null, CAT_PARENT_CAT + " = ?",
				new String[] { String.valueOf(budgetId) }, null, null,
				CAT_TILE_ORDINAL_NO);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Category cat = mapCategory(cursor, parentCategory);
				result.add(cat);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	private int findPeriodFor(Calendar date) {

		SQLiteDatabase db = getReadableDatabase();

		Cursor cursor = db.query(PERIODS, new String[] { PER_ID }, "? BETWEEN "
				+ PER_FROM + " AND " + PER_TO,
				new String[] { String.valueOf(date.getTimeInMillis() / 1000) },
				null, null, null);

		if (cursor.moveToFirst()) {
			int periodId = cursor.getInt(cursor.getColumnIndex(PER_ID));
			cursor.close();
			return periodId;
		} else {
			cursor.close();
			insertPeriodFor(date);
			return findPeriodFor(date);
		}

	}

	private void insertPeriodFor(final Calendar date) {

		Calendar dateFrom = Calendar.getInstance();
		dateFrom.setTime(date.getTime());
		Calendar dateTo = Calendar.getInstance();
		dateTo.setTime(date.getTime());

		dateFrom.set(Calendar.DAY_OF_MONTH, 1);
		dateFrom.set(Calendar.HOUR_OF_DAY, 0);
		dateFrom.set(Calendar.MINUTE, 0);
		dateFrom.set(Calendar.SECOND, 0);
		int daysInMonth = date.getActualMaximum(Calendar.DAY_OF_MONTH);
		dateTo.set(Calendar.DAY_OF_MONTH, daysInMonth);
		dateTo.set(Calendar.HOUR_OF_DAY, 23);
		dateTo.set(Calendar.MINUTE, 59);
		dateTo.set(Calendar.SECOND, 59);

		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(PER_FROM, dateFrom.getTimeInMillis() / 1000);
		values.put(PER_TO, dateTo.getTimeInMillis() / 1000);
		db.insert(PERIODS, null, values);

	}

	public List<DetailData> findDetailsFor(Category category, Calendar date) {

		List<DetailData> result = new ArrayList<DetailData>();

		int periodId = findPeriodFor(date);
		int categoryId = category.getId();

		SQLiteDatabase db = getReadableDatabase();

		Cursor cursor = db.query(
				EXPENSES,
				new String[] { EXP_ID, EXP_DATE, EXP_AMOUNT, EXP_COMMENT },
				EXP_PERIOD_ID + " = ? AND " + EXP_CATEGORY + " = ?",
				new String[] { String.valueOf(periodId),
						String.valueOf(categoryId) }, null, null, EXP_DATE);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Calendar detailDate = Calendar.getInstance();
				int dateInSeconds = cursor.getInt(cursor
						.getColumnIndex(EXP_DATE));
				detailDate.setTimeInMillis(dateInSeconds * 1000L);
				int amount = cursor.getInt(cursor.getColumnIndex(EXP_AMOUNT));
				int dataId = cursor.getInt(cursor.getColumnIndex(EXP_ID));
				String comment = cursor.getString(cursor
						.getColumnIndex(EXP_COMMENT));

				DetailData detailData = new DetailData(dataId, detailDate,
						amount, comment);
				result.add(detailData);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	private int findLimitFor(int categoryId, int periodId) {

		SQLiteDatabase db = getReadableDatabase();

		Cursor cursor = db.query(
				LIMITS,
				new String[] { LIM_LIMIT },
				LIM_PERIOD_ID + " = ? AND " + LIM_CATEGORY + " = ?",
				new String[] { String.valueOf(periodId),
						String.valueOf(categoryId) }, null, null, null);
		int limit = 0;
		if (cursor.moveToFirst()) {
			limit = cursor.getInt(cursor.getColumnIndex(LIM_LIMIT));
		}
		cursor.close();
		return limit;
	}

	private boolean hasLimitFor(int categoryId, int periodId) {

		SQLiteDatabase db = getReadableDatabase();

		Cursor cursor = db.query(
				LIMITS,
				new String[] { LIM_LIMIT },
				LIM_PERIOD_ID + " = ? AND " + LIM_CATEGORY + " = ?",
				new String[] { String.valueOf(periodId),
						String.valueOf(categoryId) }, null, null, null);
		boolean result = false;
		if (cursor.moveToFirst()) {
			result = true;
		}
		cursor.close();
		return result;
	}

	public Map<Category, Integer> findLimitsForAllCategories() {

		Map<Category, Integer> result = new HashMap<Category, Integer>();

		int periodId = findPeriodFor(Calendar.getInstance());
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(LIMITS,
				new String[] { LIM_CATEGORY, LIM_LIMIT }, LIM_PERIOD_ID
						+ " = ?", new String[] { String.valueOf(periodId) },
				null, null, null);
		List<Category> allCategories = findAllCategories();
		for (Category category : allCategories) {
			result.put(category, 0);
		}
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				int limit = cursor.getInt(cursor.getColumnIndex(LIM_LIMIT));
				int categoryId = cursor.getInt(cursor
						.getColumnIndex(LIM_CATEGORY));
				Category category = allCategories.get(allCategories
						.indexOf(new Category(categoryId, "", 0)));
				;
				result.put(category, limit);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	public void saveLimits(Map<Category, Integer> newLimits) {
		int periodId = findPeriodFor(Calendar.getInstance());
		for (Category category : newLimits.keySet()) {
			insertOrUpdate(category, newLimits.get(category), periodId);
		}
	}

	private void insertOrUpdate(Category category, Integer limit, int periodId) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(
				LIMITS,
				LIM_PERIOD_ID + " = ? AND " + LIM_CATEGORY + " = ?",
				new String[] { String.valueOf(periodId),
						String.valueOf(category.getId()) });

		ContentValues values = new ContentValues();
		values.put(LIM_PERIOD_ID, periodId);
		values.put(LIM_CATEGORY, category.getId());
		values.put(LIM_LIMIT, limit);
		try {
			db.insertOrThrow(LIMITS, null, values);
		} catch (SQLException e) {
			db.update(
					LIMITS,
					values,
					LIM_PERIOD_ID + " = ? AND " + LIM_CATEGORY + " = ? ",
					new String[] { String.valueOf(periodId),
							String.valueOf(category.getId()) });
		}
	}

	public int findLimitFor(Category category, Calendar date) {
		int periodId = findPeriodFor(date);
		int categoryId = category.getId();
		int limit = findLimitFor(categoryId, periodId);
		return limit;
	}

	public void saveCategory(String categoryName, int limit, int budgetId) {

		SQLiteDatabase db = getWritableDatabase();

		int ordinalNumber = countCategoriesOrdinalNumber();

		ContentValues values = new ContentValues();
		values.put(CAT_CATEGORY, categoryName);
		values.put(CAT_PARENT_CAT, budgetId);
		db.insert(CATEGORIES, null, values);
		int categoryId = findCategoryId(categoryName);
		Map<Category, Integer> newLimits = new HashMap<Category, Integer>();
		newLimits.put(new Category(categoryId, categoryName, ordinalNumber),
				limit);
		saveLimits(newLimits);
	}

	private int countCategoriesOrdinalNumber() {
		SQLiteDatabase db = getReadableDatabase();

		Cursor ordinalCursor = db.query(CATEGORIES,
				new String[] { "COUNT(*) as ordinalNumber" }, null, null, null,
				null, null);

		int ordinalNumber = 0;
		if (ordinalCursor.moveToFirst()) {
			ordinalNumber = ordinalCursor.getInt(ordinalCursor
					.getColumnIndex("ordinalNumber"));
		}
		ordinalCursor.close();
		return ordinalNumber;
	}

	public void updateCategory(Category category, String newName) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CAT_CATEGORY, newName);
		db.update(CATEGORIES, values, CAT_ID + " = ?",
				new String[] { String.valueOf(category.getId()) });
	}

	public void updateBudget(Category category, String newName) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BUD_TYPE, newName);
		db.update(BUDGET_TYPES, values, BUD_ID + " = ?",
				new String[] { String.valueOf(category.getId()) });
	}

	public void updateLimit(Category category, Calendar date, int limit) {
		int periodId = findPeriodFor(date);
		insertOrUpdate(category, limit, periodId);
	}

	public void updateExpense(ExpenseData expenseData) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(EXP_AMOUNT, expenseData.getAmount());
		values.put(EXP_DATE, expenseData.getDate().getTimeInMillis() / 1000);
		values.put(EXP_COMMENT, expenseData.getComment());
		int periodId = findPeriodFor(expenseData.getDate());
		values.put(EXP_PERIOD_ID, periodId);
		db.update(EXPENSES, values, EXP_ID + " = ?",
				new String[] { String.valueOf(expenseData.getDataId()) });
	}

	public void deleteCategory(Category category) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(CATEGORIES, CAT_ID + " = ?",
				new String[] { String.valueOf(category.getId()) });

	}

	public void deleteBudget(Category budget) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(BUDGET_TYPES, BUD_ID + " = ?",
				new String[] { String.valueOf(budget.getId()) });

	}

	public Balance findBalanceFor(Category category, Calendar date) {
		Balance balance = new Balance(0, 0);

		SQLiteDatabase db = getReadableDatabase();

		int periodId = findPeriodFor(date);
		int categoryId = category.getId();
		Cursor expensesCursor = db.query(
				EXPENSES,
				new String[] { "SUM(" + EXP_AMOUNT + ") as DEBIT" },
				EXP_CATEGORY + " = ? AND " + EXP_PERIOD_ID + " = ?",
				new String[] { String.valueOf(categoryId),
						String.valueOf(periodId) }, null, null, null);
		if (expensesCursor.moveToFirst()) {
			int debit = expensesCursor.getInt(expensesCursor
					.getColumnIndex("DEBIT"));
			Cursor limitsCursor = db.query(
					LIMITS,
					new String[] { LIM_LIMIT },
					LIM_CATEGORY + " = ? AND " + LIM_PERIOD_ID + " = ?",
					new String[] { String.valueOf(categoryId),
							String.valueOf(periodId) }, null, null, null);
			if (limitsCursor.moveToFirst()) {
				int limit = limitsCursor.getInt(limitsCursor
						.getColumnIndex(LIM_LIMIT));
				balance = new Balance(limit, debit);
			} else {
				balance = new Balance(0, debit);
			}
			limitsCursor.close();
		}
		expensesCursor.close();
		return balance;
	}

	public long saveBudget(String name) {

		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BUD_TYPE, name);
		return db.insert(BUDGET_TYPES, null, values);
	}

	public List<Category> findAllBudgets() {

		SQLiteDatabase db = getReadableDatabase();

		List<Category> result = new ArrayList<Category>();
		Cursor cursor = db.query(BUDGET_TYPES, new String[] { BUD_ID, BUD_TYPE,
				BUD_TILE_ORDINAL_NO }, BUD_ID + " <> ? ", new String[] { "1" },
				null, null, BUD_TILE_ORDINAL_NO);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				int budId = cursor.getInt(cursor.getColumnIndex(BUD_ID));
				String name = cursor.getString(cursor.getColumnIndex(BUD_TYPE));
				int ordinal = cursor.getInt(cursor
						.getColumnIndex(BUD_TILE_ORDINAL_NO));
				result.add(new BudgetCategory(budId, name, ordinal));
				cursor.moveToNext();
			}
		}
		cursor.close();
		return result;
	}

	public Balance findTotalFor(Calendar date, Category budget) {
		int periodId = findPeriodFor(date);

		List<Category> categoriesForBudget = findCategoriesFor(budget);

		int limit = 0;
		int debit = 0;
		if (categoriesForBudget.size() > 0) {
			SQLiteDatabase db = getReadableDatabase();
			Cursor cursor = db.query(LIMITS, new String[] { "SUM(" + LIM_LIMIT
					+ ") AS TOTAL_LIMIT" }, LIM_PERIOD_ID + " = ? AND "
					+ LIM_CATEGORY + " IN ("
					+ joinQuestionMarks(categoriesForBudget) + ")",
					prepareCategoriesIn(periodId, categoriesForBudget), null,
					null, null);
			if (cursor.moveToFirst() && !cursor.isAfterLast()) {
				limit = cursor.getInt(cursor.getColumnIndex("TOTAL_LIMIT"));
			}
			cursor.close();

			Cursor qCursor = db.query(EXPENSES, new String[] { "SUM("
					+ EXP_AMOUNT + ") AS TOTAL_DEBIT" }, EXP_PERIOD_ID
					+ " = ? AND " + EXP_CATEGORY + " IN ("
					+ joinQuestionMarks(categoriesForBudget) + ")",
					prepareCategoriesIn(periodId, categoriesForBudget), null,
					null, null);
			if (qCursor.moveToFirst() && !qCursor.isAfterLast()) {
				debit = qCursor.getInt(qCursor.getColumnIndex("TOTAL_DEBIT"));
			}
			qCursor.close();
		}
		return new Balance(limit, debit, true);
	}

	private String[] prepareCategoriesIn(int periodId,
			List<Category> categoriesForBudget) {
		List<String> params = new ArrayList<String>();
		params.add(String.valueOf(periodId));
		for (Category category : categoriesForBudget) {
			params.add(String.valueOf(category.getId()));
		}
		return params.toArray(new String[] {});
	}

	private String joinQuestionMarks(List<Category> categoriesForBudget) {
		String questionMarks = "";
		for (Category category : categoriesForBudget) {
			questionMarks += "?, ";
		}
		return questionMarks.substring(0, questionMarks.length() - 2);
	}

	public void updateCategoryWith(Category category, Category budget) {
		int budgetId = budget.getId();
		int categoryId = category.getId();

		SQLiteDatabase db = getWritableDatabase();

		ContentValues categoryValues = new ContentValues();
		categoryValues.put(CAT_PARENT_CAT, budgetId);
		db.update(CATEGORIES, categoryValues, CAT_ID + " = ?",
				new String[] { String.valueOf(categoryId) });
	}

	public void savePreferences(Preferences prefBuilder) {
		SQLiteDatabase db = getWritableDatabase();

		for (String key : prefBuilder.keys()) {
			ContentValues values = new ContentValues();
			values.put(PRE_KEY, key);
			values.put(PRE_VALUE, prefBuilder.get(key));
			try {
				db.insertOrThrow(PREFERENCES, null, values);
			} catch (SQLException exception) {
				db.update(PREFERENCES, values, PRE_KEY + " = ?",
						new String[] { key });
			}
		}
	}

	public String findPreference(String key) {
		SQLiteDatabase db = getReadableDatabase();

		String value = "";
		Cursor cursor = db.query(PREFERENCES, new String[] { PRE_VALUE },
				PRE_KEY + " = ?", new String[] { key }, null, null, null);
		if (cursor.moveToFirst()) {
			value = cursor.getString(cursor.getColumnIndex(PRE_VALUE));
		}
		cursor.close();
		return value;
	}

	public void deleteExpense(int expenseId) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(EXPENSES, EXP_ID + " = ?",
				new String[] { String.valueOf(expenseId) });
	}

	public void updateCategory(Category updatedCategory) {
		SQLiteDatabase db = getWritableDatabase();

		int parentId = updatedCategory.getParentCategory().getId();
		if (!"DEFAULT"
				.equals(updatedCategory.getParentCategory().getCategory())) {
			ContentValues budgetValues = new ContentValues();
			budgetValues.put(BUD_TYPE, updatedCategory.getParentCategory()
					.getCategory());
			db.update(BUDGET_TYPES, budgetValues, BUD_ID + " = ?",
					new String[] { String.valueOf(parentId) });
		}

		int countCategoriesOrdinalNumber = countCategoriesOrdinalNumber();

		ContentValues values = new ContentValues();
		values.put(CAT_CATEGORY, updatedCategory.getCategory());
		values.put(CAT_PARENT_CAT, parentId);
		values.put(CAT_TILE_ORDINAL_NO, countCategoriesOrdinalNumber);
		db.update(CATEGORIES, values, CAT_ID + " = ?",
				new String[] { String.valueOf(updatedCategory.getId()) });
	}

	public void updateBudgetsOrder(List<Category> budgets) {
		SQLiteDatabase db = getWritableDatabase();

		for (Category budget : budgets) {
			ContentValues values = new ContentValues();
			values.put(BUD_TILE_ORDINAL_NO, budget.getOrdinalNumber());
			db.update(BUDGET_TYPES, values, BUD_ID + " = ?",
					new String[] { String.valueOf(budget.getId()) });
		}

	}

	public void updateCategoriesOrder(List<Category> categories) {
		SQLiteDatabase db = getWritableDatabase();

		for (Category category : categories) {
			ContentValues values = new ContentValues();
			values.put(CAT_TILE_ORDINAL_NO, category.getOrdinalNumber());
			db.update(CATEGORIES, values, CAT_ID + " = ?",
					new String[] { String.valueOf(category.getId()) });
		}
	}

	public Category findCategory(int categoryId) {

		SQLiteDatabase db = getReadableDatabase();

		Category category = null;
		Cursor cursor = db.query(CATEGORIES, new String[] { CAT_CATEGORY,
				CAT_PARENT_CAT, CAT_TILE_ORDINAL_NO }, CAT_ID + " = ?",
				new String[] { String.valueOf(categoryId) }, null, null, null);
		if (cursor.moveToFirst() && !cursor.isAfterLast()) {
			String name = cursor.getString(cursor.getColumnIndex(CAT_CATEGORY));
			int parentId = cursor.getInt(cursor.getColumnIndex(CAT_PARENT_CAT));
			int ordinal = cursor.getInt(cursor
					.getColumnIndex(CAT_TILE_ORDINAL_NO));

			Category parent = null;
			if (parentId != NO_PARENT) {
				Cursor parentCursor = db.query(BUDGET_TYPES, new String[] {
						BUD_TYPE, BUD_TILE_ORDINAL_NO }, BUD_ID + " = ?",
						new String[] { String.valueOf(parentId) }, null, null,
						null);
				if (parentCursor.moveToFirst() && !parentCursor.isAfterLast()) {
					String parentName = parentCursor.getString(parentCursor
							.getColumnIndex(BUD_TYPE));
					int parentOrdinal = parentCursor.getInt(parentCursor
							.getColumnIndex(BUD_TILE_ORDINAL_NO));
					parent = new BudgetCategory(parentId, parentName,
							parentOrdinal);
				}
				parentCursor.close();
			}
			category = new Category(categoryId, name, parent, ordinal);
		}

		cursor.close();

		return category;
	}

	public void propagateLimitToNextMonthIfNecessary(Category category,
			Calendar periodShown) {
		int periodId = findPeriodFor(periodShown);
		boolean hasLimitFor = hasLimitFor(category.getId(), periodId);

		if (hasLimitFor) {
			return;
		}

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.MONTH, -1);
		int limitForYesterday = findLimitFor(category, yesterday);
		updateLimit(category, periodShown, limitForYesterday);
	}
}

class CategoryIdIsMissing extends RuntimeException {

	public CategoryIdIsMissing(String message) {
		super(message);
	}

}