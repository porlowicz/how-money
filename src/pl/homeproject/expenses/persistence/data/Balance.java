package pl.homeproject.expenses.persistence.data;

public class Balance {

	private final int limit;
	private final int debit;
	private final boolean isBudgetType;
	
	public Balance(int limit, int debit, boolean isBudgetType) {
		this.limit = limit;
		this.debit = debit;
		this.isBudgetType = isBudgetType;
	}
	
	public Balance(int limit, int debit) {
		this(limit, debit, false);
	}
	
	public int getLimit() {
		return limit;
	}
	public int getDebit() {
		return debit;
	}
	public int getBalance() {
		return limit - debit;
	}

	public String getLimitStr() {
		return String.valueOf(limit);
	}
	public String getDebitStr() {
		return String.valueOf(debit);
	}
	public String getBalanceStr() {
		return String.valueOf(limit - debit);
	}

	public boolean isBudgetType() {
		return isBudgetType;
	}

}
