package pl.homeproject.expenses.persistence.data;

public class BudgetCategory extends Category{

	public BudgetCategory(int budId, String name, int ordinal) {
		super(budId, name, ordinal);
	}

	@Override
	public String getCategory() {
		return super.getCategory().toUpperCase();
	}
	
}
