package pl.homeproject.expenses.persistence.data;

import java.io.Serializable;

import pl.homeproject.expenses.view.helper.StringControl;

public class Category implements Serializable{
	
	private final int id;
	private final String category;
	private final Category parentCategory;
	private final int ordinalNumber;
	
	public Category(int id, String category, Category parentCategory, int ordinalNumber) {
		this.id = id;
		this.category = category;
		this.parentCategory = parentCategory;
		this.ordinalNumber = ordinalNumber;
	}

	public Category(int id, String category, int orderNumber){
		this.id = id;
		this.category = category;
		this.ordinalNumber = orderNumber;
		this.parentCategory = null;		
	}
	
	public int getId() {
		return id;
	}
	public String getCategory() {
		return category;
	}
	
	public String getLimitedCategoryName(int limit) {
		return getLimitedCategoryName(limit, 1);
	}
	
	public String getLimitedCategoryName(int limit, int numberOfRows) {
		String name = category;
		String result = "";
		for(int i = 0; i < numberOfRows; i++){
			if (name.length() > limit) {
				String subName = name.substring(0, limit);
				int cutPoint = subName.lastIndexOf(" ");
				if(cutPoint < 0){
					cutPoint = limit;
				}
				result +=  name.substring(0, cutPoint) + "\n";
				name = name.substring(cutPoint);
			}else{
				result += name;
				return result;
			}
		}
		return StringControl.cut(result, result.length()-4);
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this){
			return true;
		}
		if(o instanceof Category){
			Category category = (Category) o;
			return id == category.id;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Integer.valueOf(id).hashCode();
	}

	public Category getParentCategory() {
		return parentCategory;
	}

	public int getOrdinalNumber() {
		return ordinalNumber;
	}
}
