package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.handler.KeyboardHider;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

public class AbstractPopupWindow extends PopupWindow {

	private final View layout;
	private Context context;

	public AbstractPopupWindow(final Context context, int layoutId) {
		super(context);
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layout = inflater.inflate(layoutId, null);
		setContentView(layout);

		layout.setClickable(true);
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				KeyboardHider.hide(context, layout);
			}
		});

		setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
		setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
		setFocusable(true);

		setTouchInterceptor(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				KeyboardHider.hide(context, getLayout());
				return false;
			}
		});
	}

	public void show() {
		showAtLocation(layout, Gravity.CENTER | Gravity.TOP, 0, 0);
	}

	public View findViewById(int id) {
		return layout.findViewById(id);
	}

	public View getLayout() {
		return layout;
	}
}
