package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.KeyboardHider;
import pl.homeproject.expenses.register.data.Comment;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class AddCommentPopupWindow extends AbstractPopupWindow {

	public AddCommentPopupWindow(final Context context, final Comment comment) {
		super(context, R.layout.add_new_comment);

		final TextView commentContent = (TextView) getLayout()
				.findViewById(R.id.commentContent);
		commentContent.setText(comment.getContent());
		
		getLayout().findViewById(R.id.addSaveButton).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						comment.setContent(commentContent.getText().toString());
						KeyboardHider.hide(context, getLayout());
						AddCommentPopupWindow.this.dismiss();
					}

				});

		getLayout().findViewById(R.id.addCancelButton).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						KeyboardHider.hide(context, getLayout());
						AddCommentPopupWindow.this.dismiss();
					}
				});
		getLayout().findViewById(R.id.commentClear).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						commentContent.setText("");
						comment.setContent("");
					}
				});
	}

}
