package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.view.DecimalEditText;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

public class AddNewTilePopupWindow extends AbstractPopupWindow {

	public AddNewTilePopupWindow(final Context context) {
		super(context, R.layout.add_new_tile);
		
		final CheckBox makeBudgetFolderCheckbox = (CheckBox) getContentView().findViewById(R.id.makeBudgetFolderCheckbox);
		TextView budgetTypeName = (TextView) getContentView().findViewById(R.id.budgetTypeName);
		if(context instanceof SubMainActivity){
			getContentView().findViewById(R.id.makeFolderLabel).setVisibility(View.INVISIBLE);
			makeBudgetFolderCheckbox.setVisibility(View.INVISIBLE);
			budgetTypeName.setText(R.string.add_new_category_label);
		}else{
			budgetTypeName.setText(R.string.add_new_category_or_budget_label);
			makeBudgetFolderCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					View addCategoryLimit = getContentView().findViewById(R.id.newCategoryLimitAmoutLayour);
					if(isChecked){
						addCategoryLimit.setVisibility(View.INVISIBLE);
					}else{
						addCategoryLimit.setVisibility(View.VISIBLE);
					}
				}
			});
		}
		View saveNewTile = getContentView().findViewById(R.id.saveNewTile);
		View cancelNewTile = getContentView().findViewById(R.id.cancelNewTile);
		saveNewTile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View saveButtonView) {
				
				EditText addCategoryName = (EditText) getContentView().findViewById(R.id.addCategoryName);
				String categoryName = addCategoryName.getText().toString();
				
				boolean makeFolder = makeBudgetFolderCheckbox.isChecked();
				
				Persister persister = Persister.getInstance(context);
				if(!makeFolder){
					DecimalEditText addCategoryLimit = (DecimalEditText) getContentView().findViewById(R.id.addCategoryLimit);
					int limit = addCategoryLimit.getFormattedText();
					persister.saveCategory(categoryName, limit, ((MainActivity)context).getBudgetTypeId());
				}else{
					persister.saveBudget(categoryName);
				}
				
				((MainActivity) context).updateTiles();
				dismiss();
			}
		});
		cancelNewTile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View cancelButtonView) {
				
				dismiss();
			}
		});
	}
	
}
