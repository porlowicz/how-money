package pl.homeproject.expenses.popupWindow;

import java.util.List;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.CategoryLimitedLengthTextView;
import pl.homeproject.expenses.view.DecimalEditText;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class CreateFolderFromDropPopupWindow extends AbstractPopupWindow {

	public CreateFolderFromDropPopupWindow(final Context context, final View targetView, final View draggedView) {
		super(context, R.layout.create_folder_from_drop);
		
		View saveNewTile = getContentView().findViewById(R.id.saveNewTile);
		View cancelNewTile = getContentView().findViewById(R.id.cancelNewTile);
		saveNewTile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View saveButtonView) {
				
				EditText addCategoryName = (EditText) getContentView().findViewById(R.id.newFolderName);
				String categoryName = addCategoryName.getText().toString();
				
				Persister persister = Persister.getInstance(context);
				long newBudgetId = persister.saveBudget(categoryName);
				
				List<Category> allBudgets = persister.findAllBudgets();
				Category newBudgetFolder = null;
				for (Category budget : allBudgets) {
					if(newBudgetId == budget.getId()){
						newBudgetFolder = budget;
						break;
					}
				}

				CategoryLimitedLengthTextView targetTileCategory = (CategoryLimitedLengthTextView) targetView.findViewById(R.id.tileCategory);
				CategoryLimitedLengthTextView draggedTileCategory = (CategoryLimitedLengthTextView) draggedView.findViewById(R.id.tileCategory);
				
				persister.updateCategoryWith(targetTileCategory.getCategory(), newBudgetFolder);
				persister.updateCategoryWith(draggedTileCategory.getCategory(), newBudgetFolder);
				
				((MainActivity) context).updateTiles();
				dismiss();
			}
		});
		cancelNewTile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View cancelButtonView) {
				
				dismiss();
				((MainActivity) context).updateTiles();
				dismiss();
			}
		});
		
	}

}
