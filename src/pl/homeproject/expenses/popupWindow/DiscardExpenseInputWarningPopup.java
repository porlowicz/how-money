package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.view.DecimalEditText;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class DiscardExpenseInputWarningPopup extends AbstractPopupWindow {

	public DiscardExpenseInputWarningPopup(final Context context, final DecimalEditText view) {
		super(context, R.layout.discard_expense_amount_warning);

		TextView warningText = (TextView) getLayout().findViewById(R.id.warningText);
		
		String warningFormattedText = String
				.format(context
						.getString(R.string.discard_expense_amount_warning_text),
						DecimalFormatter.formatDecimalNumber(String.valueOf(view
								.getFormattedText())), Preferences.getCurrencySymbol(context));
		warningText.setText(warningFormattedText);

		View warningGoBackAnywayButton = getLayout().findViewById(R.id.warningGoBackAnywayButton);
		warningGoBackAnywayButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				view.setText("");
				((Activity)context).onBackPressed();
			}
		});
		
		View warningStayOnPageButton = getLayout().findViewById(R.id.warningStayOnPageButton);
		warningStayOnPageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
}
