package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.SubMainActivity;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.helper.StringControl;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class ModifyBudgetNamePopupWindow extends AbstractPopupWindow{

	public ModifyBudgetNamePopupWindow(final Context context, final Category category) {
		super(context, R.layout.modify_category_name_layout);
		
		EditText categoryNameEdit = (EditText) getLayout().findViewById(R.id.categoryNameEdit);
		categoryNameEdit.setText(StringControl.firstLetterUpperCase(category.getCategory()));
		categoryNameEdit.setSelection(category.getCategory().length());
		
		final Persister persister = Persister.getInstance(context);
		
		getLayout().findViewById(R.id.addSaveButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				EditText categoryNameEdit = (EditText) getLayout().findViewById(R.id.categoryNameEdit);
				String newCategoryName = categoryNameEdit.getText().toString();
				persister.updateBudget(category, newCategoryName);
				
				ModifyBudgetNamePopupWindow.this.dismiss();
				Category newBudget = new Category(category.getId(), newCategoryName, category.getOrdinalNumber());
				((SubMainActivity) context).setBudget(newBudget);
				((SubMainActivity) context).updateActionBar();
				
			}
		});
		getLayout().findViewById(R.id.addCancelButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ModifyBudgetNamePopupWindow.this.dismiss();
			}
		});

	}
}
