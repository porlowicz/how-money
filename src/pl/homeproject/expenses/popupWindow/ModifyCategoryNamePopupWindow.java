package pl.homeproject.expenses.popupWindow;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class ModifyCategoryNamePopupWindow extends AbstractPopupWindow{

	public ModifyCategoryNamePopupWindow(final Context context, final Category category) {
		super(context, R.layout.modify_category_name_layout);
		
		EditText categoryNameEdit = (EditText) getLayout().findViewById(R.id.categoryNameEdit);
		categoryNameEdit.setText(category.getCategory());
		categoryNameEdit.setSelection(category.getCategory().length());
		
		final Persister persister = Persister.getInstance(context);
		
		getLayout().findViewById(R.id.addSaveButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				EditText categoryNameEdit = (EditText) getLayout().findViewById(R.id.categoryNameEdit);
				String newCategoryName = categoryNameEdit.getText().toString();
				persister.updateCategory(category, newCategoryName);
				
				ModifyCategoryNamePopupWindow.this.dismiss();
				((DetailsActivity) context).updateCategory(newCategoryName);
			}
		});
		getLayout().findViewById(R.id.addCancelButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ModifyCategoryNamePopupWindow.this.dismiss();
			}
		});
		
	}
}
