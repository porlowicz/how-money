package pl.homeproject.expenses.popupWindow;

import java.util.Calendar;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.fragment.DateHost;
import pl.homeproject.expenses.fragment.DatePickerFragment;
import pl.homeproject.expenses.handler.DateChangedListener;
import pl.homeproject.expenses.handler.KeyboardHider;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.register.data.DateFormat;
import pl.homeproject.expenses.register.data.DetailData;
import pl.homeproject.expenses.register.data.ExpenseData;
import pl.homeproject.expenses.view.CategoryTextView;
import pl.homeproject.expenses.view.DecimalEditText;
import pl.homeproject.expenses.view.DetailView;
import pl.homeproject.expenses.view.helper.StringControl;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ModifyExpensePopupWindow extends AbstractPopupWindow implements
		DateChangedListener, DateHost {

	private TextView commentPreviewTextView = null;
	private EditText commentEdit = null;

	private DetailView detailView;
	private DetailsActivity detailsActivity;
	private int amount;

	public ModifyExpensePopupWindow(final DetailsActivity context,
			final Category tileCategoryText, final DetailView view) {
		super(context, R.layout.add_expense_layout);

		this.detailsActivity = context;
		detailView = view;

		if (detailsActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			adjustDeleteButtonTo(Configuration.ORIENTATION_LANDSCAPE);
		}

		final Persister persister = Persister.getInstance(context);

		setDate(view.getDetailData().getDetailDate());

		setComment(view);

		EditText expenseAmountDetail = (EditText) getLayout().findViewById(
				R.id.expenseAmountDetail);
		amount = view.getDetailData().getAmount();
		final StringBuffer stringAmount;
		if (amount == 0) {
			stringAmount = new StringBuffer("000");
		} else {
			stringAmount = new StringBuffer(String.valueOf(amount));
		}

		stringAmount.insert(stringAmount.length() - 2, ".");
		expenseAmountDetail.setHint(String.format("%s (old: %s)",
				expenseAmountDetail.getHint(), stringAmount.toString()));

		TextView currencySymbolContainer = (TextView) getLayout().findViewById(
				R.id.currencySymbolContainer);
		currencySymbolContainer.setText(Preferences.getCurrencySymbol(context));

		final CategoryTextView categoryHint = (CategoryTextView) getLayout()
				.findViewById(R.id.categoryHint);
		categoryHint.setCategory(tileCategoryText);

		View datePickerLayoutField = getLayout().findViewById(
				R.id.datePickerLayoutField);
		datePickerLayoutField.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				DialogFragment newFragment = new ModifyDatePickerFragment(
						ModifyExpensePopupWindow.this);
				newFragment.show(((ActionBarActivity) context)
						.getSupportFragmentManager(), "datePicker");
			}
		});

		getLayout().findViewById(R.id.addSaveButton).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						TextView dateOfSpending = (TextView) getLayout()
								.findViewById(R.id.dateOfSpending);
						Calendar date = DateFormat.parseDate(dateOfSpending
								.getText().toString());
						date.set(Calendar.HOUR, detailView.getDetailData()
								.getDetailDate().get(Calendar.HOUR));
						date.set(Calendar.MINUTE, detailView.getDetailData()
								.getDetailDate().get(Calendar.MINUTE));
						date.set(Calendar.SECOND, detailView.getDetailData()
								.getDetailDate().get(Calendar.SECOND));
						if (date.get(Calendar.DAY_OF_MONTH) != detailView
								.getDetailData().getDetailDate()
								.get(Calendar.DAY_OF_MONTH)
								|| date.get(Calendar.MONTH) != detailView
										.getDetailData().getDetailDate()
										.get(Calendar.MONTH)
								|| date.get(Calendar.YEAR) != detailView
										.getDetailData().getDetailDate()
										.get(Calendar.YEAR)) {
							date.set(Calendar.HOUR, 23);
							date.set(Calendar.MINUTE, 59);
							date.set(Calendar.SECOND, 59);
						}

						DecimalEditText expenseAmount = (DecimalEditText) getLayout()
								.findViewById(R.id.expenseAmountDetail);

						int expenseAmountValue;
						if (expenseAmount.getText().length() == 0) {
							expenseAmountValue = amount;
						} else {
							expenseAmountValue = expenseAmount
									.getFormattedText();
						}

						Category category = categoryHint.getCategory();

						String comment;
						if (commentEdit.getVisibility() == View.VISIBLE) {
							comment = commentEdit.getText().toString();
						} else {
							comment = view.getDetailData().getComment();
						}

						ExpenseData expense = new ExpenseData(detailView
								.getDetailData().getDataId(), category, date,
								expenseAmountValue, comment);
						persister.updateExpense(expense);

						DetailData modifiedDetailData = new DetailData(view
								.getDetailData().getDataId(), date,
								expenseAmountValue);
						view.setDetailData(modifiedDetailData);
						TextView detailAmount = (TextView) view
								.findViewById(R.id.detailAmount);
						if (detailAmount == null) {
							detailAmount = (TextView) view
									.findViewById(R.id.detailTopRowAmount);
						}
						detailAmount.setText(String.valueOf(expenseAmount
								.getFormattedText()));
						TextView detailDate = (TextView) view
								.findViewById(R.id.detailDate);
						if (detailDate == null) {
							detailDate = (TextView) view
									.findViewById(R.id.detailTopRowDate);
						}
						detailDate.setText(DateFormat.formatDate(date.getTime()));

						KeyboardHider.hide(context, getLayout());
						ModifyExpensePopupWindow.this.dismiss();
						((DetailsActivity) context).showDetails();

					}
				});
		getLayout().findViewById(R.id.addCancelButton).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						KeyboardHider.hide(context, getLayout());
						ModifyExpensePopupWindow.this.dismiss();
					}
				});

		getLayout().findViewById(R.id.deleteTransactionButton)
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								switch (which) {
								case DialogInterface.BUTTON_POSITIVE:
									Persister.getInstance(context)
											.deleteExpense(
													detailView.getDetailData()
															.getDataId());
									detailsActivity.showDetails();
									ModifyExpensePopupWindow.this.dismiss();
									break;
								}
							}
						};
						KeyboardHider.hide(context, getLayout());

						TextView dateOfSpending = (TextView) getContentView()
								.findViewById(R.id.dateOfSpending);
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								context);
						alertDialogBuilder.setMessage(String.format(
								"Delete transaction on\n%s\nin amount of %s?",
								dateOfSpending.getText().toString(),
								stringAmount));
						alertDialogBuilder.setPositiveButton("Yes, delete it",
								clickListener).setNegativeButton(
								"No, leave it", clickListener);
						alertDialogBuilder.show();
					}
				});
	}

	private void setComment(final DetailView view) {
		commentPreviewTextView = (TextView) getLayout().findViewById(
				R.id.commentPreviewTextView);
		if (view.getDetailData().getComment() == null
				|| "".equals(view.getDetailData().getComment().trim())) {
			commentPreviewTextView.setHint(R.string.no_comment);
		} else {
			commentPreviewTextView.setText(StringControl.cut(view
					.getDetailData().getComment(), 300));
		}

		commentEdit = (EditText) getLayout().findViewById(R.id.commentEdit);

		final View commentAcceptModify = getLayout().findViewById(
				R.id.commentAcceptModify);
		final View commentCancelModify = getLayout().findViewById(
				R.id.commentCancelModify);

		commentAcceptModify.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DetailData d = detailView.getDetailData();
				commentPreviewTextView
						.setText(commentEdit.getText().toString());
				view.setDetailData(new DetailData(d.getDataId(), d
						.getDetailDate(), d.getAmount(), commentEdit.getText()
						.toString()));
				hideEditCommentView(commentAcceptModify, commentCancelModify);
			}
		});

		commentCancelModify.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideEditCommentView(commentAcceptModify, commentCancelModify);
			}
		});

		commentPreviewTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showEditCommentView(commentAcceptModify, commentCancelModify);
			}
		});
	}

	private void hideEditCommentView(final View commentAcceptModify,
			final View commentCancelModify) {
		getLayout().post(new Runnable() {

			@Override
			public void run() {
				KeyboardHider.hide(detailsActivity, getLayout());
				commentAcceptModify.setVisibility(View.GONE);
				commentCancelModify.setVisibility(View.GONE);
				commentPreviewTextView.setVisibility(View.VISIBLE);
				commentEdit.setVisibility(View.GONE);
				commentEdit.setText("");
			}
		});
	}

	private void showEditCommentView(final View commentAcceptModify,
			final View commentCancelModify) {
		getLayout().post(new Runnable() {

			@Override
			public void run() {
				KeyboardHider.show(detailsActivity);
				commentAcceptModify.setVisibility(View.VISIBLE);
				commentCancelModify.setVisibility(View.VISIBLE);
				commentPreviewTextView.setVisibility(View.GONE);
				commentEdit.setVisibility(View.VISIBLE);
				commentEdit.setText(commentPreviewTextView.getText());
			}
		});
	}

	@Override
	public void updateDate(Calendar date) {
		TextView dateOfSpending = (TextView) getContentView().findViewById(
				R.id.dateOfSpending);
		dateOfSpending.setText(DateFormat.formatDate(date.getTime()));
	}

	@Override
	public void setDate(Calendar date) {
		DetailData detailData = detailView.getDetailData();
		detailData.getDetailDate().setTime(date.getTime());
		updateDate(date);
	}

	@Override
	public Calendar getDate() {
		DetailData detailData = detailView.getDetailData();
		return detailData.getDetailDate();
	}

	@Override
	public void show() {
		detailsActivity.addPopupWindow(this);
		super.show();
	}

	@SuppressLint("NewApi")
	public void adjustDeleteButtonTo(int orientationPortrait) {
		if (orientationPortrait == Configuration.ORIENTATION_LANDSCAPE) {
			View deleteTransactionButton = getLayout().findViewById(
					R.id.deleteTransactionButton);
			deleteTransactionButton
					.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT, 1f));
			ViewGroup parent = (ViewGroup) deleteTransactionButton.getParent();
			parent.removeView(deleteTransactionButton);
			ViewGroup addExpenseButtonsLayout = (ViewGroup) getLayout()
					.findViewById(R.id.addExpenseButtonsLayout);
			addExpenseButtonsLayout.addView(deleteTransactionButton, 0);
		} else {
			View deleteTransactionButton = getLayout().findViewById(
					R.id.deleteTransactionButton);
			deleteTransactionButton
					.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));
			ViewGroup parent = (ViewGroup) deleteTransactionButton.getParent();
			parent.removeView(deleteTransactionButton);
			ViewGroup addExpenseButtonsLayout = (ViewGroup) getLayout()
					.findViewById(R.id.addExpenseDeleteButtonLayout);
			addExpenseButtonsLayout.addView(deleteTransactionButton, 0);
		}
	}

}

@SuppressLint("ValidFragment")
class ModifyDatePickerFragment extends DatePickerFragment {

	private DateHost dateHost;

	public ModifyDatePickerFragment(DateHost dateHost) {
		this.dateHost = dateHost;
	}

	@Override
	protected DateHost getDateHost() {
		return dateHost;
	}

}
