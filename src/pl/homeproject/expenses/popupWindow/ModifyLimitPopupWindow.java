package pl.homeproject.expenses.popupWindow;

import java.util.Calendar;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.register.data.DateFormat;
import pl.homeproject.expenses.view.CategoryLimitedLengthTextView;
import pl.homeproject.expenses.view.DecimalEditText;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class ModifyLimitPopupWindow extends AbstractPopupWindow {

	public ModifyLimitPopupWindow(final Context context, final TextView limitLabel, final Category category, final int limit, final Calendar date) {
		super(context, R.layout.modify_limit_layout);
		
		final Persister persister = Persister.getInstance(context);

		TextView limitPeriod = (TextView) findViewById(R.id.limitPeriod);
		limitPeriod.setText(DateFormat.formatPeriod(date.getTime()));
		
		EditText expenseLimitDetail = (EditText) findViewById(R.id.expenseLimitDetail);
		String stringAmount = DecimalFormatter.formatDecimalNumber(limit);
		expenseLimitDetail.setHint(String.format("%s (old: %s)", expenseLimitDetail.getHint().toString(), stringAmount));
		
		TextView currencySymbolContainer = (TextView) findViewById(R.id.currencySymbolContainer);
		currencySymbolContainer.setText(Preferences.getCurrencySymbol(context));
		
		final CategoryLimitedLengthTextView categoryHint = (CategoryLimitedLengthTextView) findViewById(R.id.categoryHint);
		categoryHint.setCategory(category);
		
		findViewById(R.id.addSaveButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				DecimalEditText expenseLimitDetail = (DecimalEditText) findViewById(R.id.expenseLimitDetail);
				if(expenseLimitDetail.getVisibility() == View.VISIBLE){
					int expenseLimitValue;
					if(expenseLimitDetail.getText().length() == 0){
						expenseLimitValue = limit;
					}else{
						expenseLimitValue = expenseLimitDetail.getFormattedText();
					}
					
					persister.updateLimit(category, date, expenseLimitValue);
					
					limitLabel.setText(String.valueOf(limit));
					
				} 
				ModifyLimitPopupWindow.this.dismiss();
				((DetailsActivity) context).showDetails();
			}
		});
		findViewById(R.id.addCancelButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ModifyLimitPopupWindow.this.dismiss();
			}
		});
	}
}
