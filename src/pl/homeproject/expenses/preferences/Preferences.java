package pl.homeproject.expenses.preferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import pl.homeproject.expenses.persistence.Persister;
import android.content.Context;

public class Preferences{

	private static final String CURRENCY = "currency";

	private Map<String, String> preferences = new HashMap<String, String>();
	
	public void putCurrency(String value) {
		preferences.put(CURRENCY, value);
	}

	public Set<String> keys() {
		return preferences.keySet();
	}

	public String get(String key) {
		return preferences.get(key);
	}

	public static String getCurrencySymbol(Context context) {
		String currency = Persister.getInstance(context).findPreference(CURRENCY);
		if(currency == null || currency.trim().equals("")){
			return "";
		}
		return " " + currency;
	}

}
