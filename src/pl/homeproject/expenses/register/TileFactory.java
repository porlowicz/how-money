package pl.homeproject.expenses.register;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.model.BudgetTileViewModel;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.content.Context;
import android.graphics.Color;

public class TileFactory {

	private final Persister persister;
	
	public TileFactory(Context context) {
		persister = Persister.getInstance(context);
	}
	
	private static final float gH = 102;  // 5ED42A
	private static final float gS = 0.8f;
	private static final float gV = 0.83f;
	
	private static final float rH = 0;	// D42A2A
	private static final float rS = 0.8f;
	private static final float rV = 0.83f;
	private static final float[] redHsv = new float[]{rH,rS,rV};
	
	public static int gradientFor(Balance balanceData) {
		
		if(balanceData.getBalance() < 0){
			return Color.HSVToColor(redHsv);
		}
		int debit = balanceData.getDebit();
		int limit = balanceData.getLimit();

		if(limit == 0 && debit == 0){
			return Color.HSVToColor(new float[]{gH, gS, gV});
		}
		
		float redMargin = 10;
		float delta = ((gH-redMargin )/(float)limit) * debit;
		float[] greenHsv = new float[]{gH-delta,gS,gV};
		return Color.HSVToColor(greenHsv);
	}

	public List<TileViewModel> getTileViews() {
		
		List<TileViewModel> result = new ArrayList<TileViewModel>();
		
		List<Category> findAllCategories = persister.findStandaloneCategories();
		for (Category category : findAllCategories) {
			Balance balance = persister.findBalanceFor(category, Calendar.getInstance());
			if(!balance.isBudgetType()){
				result.add(new TileViewModel(category, balance.getLimit(), balance.getDebit(), category.getOrdinalNumber()));
			}
		}
		return result;
	}

	public List<TileViewModel> getBudgetTiles() {

		List<TileViewModel> result = new ArrayList<TileViewModel>();
		List<Category> allBudgets = persister.findAllBudgets();
		for (Category budget : allBudgets) {
			Balance totalBalance = persister.findTotalFor(Calendar.getInstance(), budget);
			result.add(new BudgetTileViewModel(budget, totalBalance.getLimit(), totalBalance.getDebit(), budget.getOrdinalNumber()));
		}
		return result;
	}

	public List<TileViewModel> getTileViewsFor(Category budget) {
		List<TileViewModel> result = new ArrayList<TileViewModel>();
		
		List<Category> findAllCategories = persister.findCategoriesFor(budget);
		for (Category category : findAllCategories) {
			Balance balance = persister.findBalanceFor(category, Calendar.getInstance());
			result.add(new TileViewModel(category, balance.getLimit(), balance.getDebit(), category.getOrdinalNumber()));
		}
		return result;
	}

}
