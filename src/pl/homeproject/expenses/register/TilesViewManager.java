package pl.homeproject.expenses.register;

import java.util.ArrayList;
import java.util.List;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.frontend.tiles.gesture.impl.OrganizeDragAndDropHandler;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.CategoryTextView;
import pl.homeproject.expenses.view.DndFrameLayoutAdapter;
import pl.homeproject.expenses.view.LockableScrollView;
import pl.homeproject.expenses.view.TileView;
import pl.homeproject.expenses.view.model.BudgetTileViewModel;
import pl.homeproject.expenses.view.model.TileViewModel;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.GridLayout;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;

public class TilesViewManager implements OnGestureListener {

	private OrganizeDragAndDropHandler organizeDragAndDropHandler;
	private MainActivity activity;
	private List<TileViewModel> tileViews;

	private GestureDetectorCompat mDetector;
	private boolean isLongTouch = false;
	private View touchedView;
	private DndFrameLayoutAdapter dndTilesGrid;
	private GridLayout tilesGrid;
	
	public TilesViewManager(MainActivity context) {
		this.activity = context;
		if(android.os.Build.VERSION.SDK_INT >= 11){
			organizeDragAndDropHandler = new OrganizeDragAndDropHandler(context);
		}else{
			// oldApi support
			mDetector = new GestureDetectorCompat(activity, this);
			mDetector.setIsLongpressEnabled(true);
			dndTilesGrid = (DndFrameLayoutAdapter) activity.findViewById(R.id.dndTilesGrid);
			tilesGrid = (GridLayout) activity.findViewById(R.id.tilesGrid);
		}
	}

	public List<View> getMainTiles() {
		TileFactory tileFactory = new TileFactory(activity);
		tileViews = tileFactory.getBudgetTiles();
		tileViews.addAll(tileFactory.getTileViews());
		List<View> resultViews = loadView(tileViews);
		return resultViews;
	}

	@SuppressLint("NewApi")
	private List<View> loadView(List<TileViewModel> tileViews) {
		List<View> result = new ArrayList<View>();
		if(android.os.Build.VERSION.SDK_INT >= 11){
			activity.findViewById(R.id.topLevelFrame).setOnDragListener(organizeDragAndDropHandler);
		}else{
			// oldApi support
		}
		final LayoutInflater inflater = activity.getLayoutInflater();
		for (final TileViewModel tileView : tileViews) {
			final View inflatedTileFrame = inflater.inflate(R.layout.tile_layout, null);
			final TileView inflatedTileView = (TileView) inflatedTileFrame.findViewById(R.id.tileTemplateView);
			inflatedTileView.setBudget(tileView instanceof BudgetTileViewModel);
			inflatedTileView.setOnClickListener(tileView.getOnClickHandler(activity));
			if(android.os.Build.VERSION.SDK_INT >= 11){
				if(inflatedTileView.isBudget()){
					organizeDragAndDropHandler.addDropTarget(inflatedTileView);
				}
				inflatedTileView.setOnDragListener(organizeDragAndDropHandler);
			}else{
				// oldApi support
			}
			
			inflatedTileView.setOnLongClickListener(new OnLongClickListener() {
				@SuppressLint("NewApi")
				@Override
				public boolean onLongClick(View v) {
					if(android.os.Build.VERSION.SDK_INT >= 11){
						Intent intent = new Intent();
						intent.putExtra("category", tileView.getCategory());
						ClipData data = ClipData.newIntent("Category to move", intent);
						v.startDrag(data, new View.DragShadowBuilder(inflatedTileFrame), v, 0);
						v.setVisibility(View.INVISIBLE);
						inflatedTileFrame.findViewById(R.id.budgetStateIndicator).setVisibility(View.INVISIBLE);
						activity.replaceAddTileWithDelete();
						activity.findViewById(R.id.addRemoveSetting).setOnDragListener(organizeDragAndDropHandler);
					}else{
						// oldApi support
					}
					return false;
				}
			});
			TextView budgetStateIndicator = (TextView) inflatedTileFrame.findViewById(R.id.budgetStateIndicator);
			budgetStateIndicator.setText(tileView.getPercentage());
			
			Drawable border_rounded_corners = activity.getResources().getDrawable(R.drawable.border_rounded_corners);
			border_rounded_corners.setColorFilter(tileView.getBackgroundColor(), Mode.MULTIPLY);
			budgetStateIndicator.setBackgroundDrawable(border_rounded_corners);
			if(inflatedTileView.isBudget()){
				budgetStateIndicator.setBackgroundColor(0x00000000);
				inflatedTileFrame.findViewById(R.id.linearLayoutWithSummary).setPadding(0, 0, 0, 10);
				MarginLayoutParams layoutParams = (MarginLayoutParams) budgetStateIndicator.getLayoutParams();
				layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, layoutParams.bottomMargin + 10);
				budgetStateIndicator.setLayoutParams(layoutParams);
			}
			inflatedTileView.setBackgroundResource(tileView.getBackgroundResource());
			
			categoryTextViewSetup(tileView, inflatedTileView, R.id.tileCategory);
			textViewSetup(String.valueOf(tileView.getLimit()), inflatedTileView, R.id.tileLimit);
			textViewSetup(String.valueOf(tileView.getDebit()), inflatedTileView, R.id.tileDebit);
			textViewSetup(String.valueOf(tileView.getBalance()), inflatedTileView, R.id.tileBalance);
			
			result.add(inflatedTileFrame);
		}		
		return result;
	}

	private void categoryTextViewSetup(TileViewModel categoryTile,
			TileView inflatedTileView, int tilecategoryId) {
		textViewSetup(categoryTile.getCategory().getCategory(), inflatedTileView, tilecategoryId);
		CategoryTextView categoryTextView = (CategoryTextView) inflatedTileView.findViewById(tilecategoryId);
		categoryTextView.setCategory(categoryTile.getCategory());
	}

	private void textViewSetup(String text,
			View inflatedTileView, int viewId) {
		TextView textView = (TextView) inflatedTileView.findViewById(viewId);
		textView.setText(text);
	}
	
	public List<View> getTilesFor(Category budget) {
		TileFactory tileFactory = new TileFactory(activity);
		List<TileViewModel> tileViews = tileFactory.getTileViewsFor(budget);
		List<View> resultViews = loadView(tileViews);
		return resultViews;
	}

	public List<TileViewModel> getTileViews() {
		return tileViews;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		dndTilesGrid.setPosition((int)e.getX(), (int)e.getY());
		dndTilesGrid.postInvalidate();
		return isLongTouch;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		ViewGroup parent = (ViewGroup) touchedView.getParent();
		ViewGroup grandParent = (ViewGroup) parent.getParent();
		
		
		dndTilesGrid.setPosition((int)e.getRawX(), (int)e.getRawY());
		dndTilesGrid.setDraggedView(parent);
		
		dndTilesGrid.postInvalidate();
		
		parent.setVisibility(View.INVISIBLE);
		isLongTouch = true;
		
		LockableScrollView topLevelView = (LockableScrollView) activity.findViewById(R.id.topLevelView);
		topLevelView.setScrollingEnabled(false);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

}
