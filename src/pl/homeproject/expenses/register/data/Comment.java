package pl.homeproject.expenses.register.data;

public class Comment {

	private String content;

	public Comment() {
		content = "";
	}
	
	public Comment(String content) {
		if(content != null){
			this.content = content;
		}else{
			this.content = "";
		}
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if(content != null){
			this.content = content;
		}
	}
	
	
}
