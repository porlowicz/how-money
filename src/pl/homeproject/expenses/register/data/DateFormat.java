package pl.homeproject.expenses.register.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateFormat {

	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
	public static final SimpleDateFormat periodFormat = new SimpleDateFormat("MMMM yyyy");
	public static final SimpleDateFormat chartMonth = new SimpleDateFormat("MMM");

	public static String formatChartMonth(Date time){
		return chartMonth.format(time);
	}
	
	/**
	 * 
	 * @param time in format "yyyy-MM-dd"
	 * @return
	 */
	public static String formatDate(Date time) {
		return dateFormat.format(time);
	}
	
	/**
	 * 
	 * @param dateOfSpending in format "yyyy-MM-dd"
	 * @return
	 */
	public static Calendar parseDate(String dateOfSpending) {
		Calendar date = Calendar.getInstance();
		try {
			date.setTime(dateFormat.parse(dateOfSpending));
			return date;
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * 
	 * @param time in format "MMMM yyyy"
	 * @return
	 */
	public static String formatPeriod(Date time){
		return periodFormat.format(time);
	}

	
}
