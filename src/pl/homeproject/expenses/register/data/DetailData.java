package pl.homeproject.expenses.register.data;

import java.util.Calendar;

public class DetailData {
	
	private final Calendar detailDate;
	private final int amount;
	private final int dataId;
	private final String comment;
	
	public DetailData(int dataId, Calendar detailDate, int amount){
		this(dataId, detailDate, amount, "");
	}
	
	public DetailData(int dataId, Calendar detailDate, int amount, String comment) {
		this.dataId = dataId;
		this.detailDate = detailDate;
		this.amount = amount;
		this.comment = comment;
	}

	public Calendar getDetailDate() {
		return detailDate;
	}

	public int getAmount() {
		return amount;
	}

	public String getFormattedDetailDate() {
		return DateFormat.formatDate(detailDate.getTime());
	}

	public int getDataId() {
		return dataId;
	}

	public String getComment() {
		return comment;
	}

}
