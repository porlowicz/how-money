package pl.homeproject.expenses.register.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pl.homeproject.expenses.DetailsActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.ModifyLimitListener;
import pl.homeproject.expenses.handler.RollExpensesHandler;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.popupWindow.AbstractPopupWindow;
import pl.homeproject.expenses.popupWindow.ModifyExpensePopupWindow;
import pl.homeproject.expenses.view.DetailView;
import pl.homeproject.expenses.view.helper.StringControl;
import android.app.Activity;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsProvider {

	private DetailsActivity context;
	private Persister persister;
	
	private static final RollExpensesHandler rollExpensesHandler = new RollExpensesHandler();
	
	public DetailsProvider(DetailsActivity context) {
		this.context = context;
		persister = Persister.getInstance(context);
	}

	public List<DetailView> getDetailsFor(final Category category, final Calendar date) {
		
		List<DetailView> result = new ArrayList<DetailView>();
		
		final int limit = persister.findLimitFor(category, date);
		TextView detailLimit = (TextView) ((Activity)context).findViewById(R.id.detailLimit);
		ModifyLimitListener modifyLimitListener = new ModifyLimitListener((Activity) context, category, limit, date, detailLimit);
		detailLimit.setOnClickListener(modifyLimitListener);
		((Activity)context).findViewById(R.id.editIconPencilLimit).setOnClickListener(modifyLimitListener);
		detailLimit.setText(String.valueOf(limit));
		
		final List<DetailData> detailsData = persister.findDetailsFor(category, date);
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		
		int sumOfDebit = 0;
		Calendar previousDate = null;
		for (int i = 0; i < detailsData.size(); i++) {
			DetailData details = detailsData.get(i);
			sumOfDebit += details.getAmount();

			if(isNewDate(previousDate, details.getDetailDate())){
				if (i + 1 < detailsData.size() && !isNewDate(details.getDetailDate(), detailsData.get(i + 1)
						.getDetailDate())) {
					previousDate = details.getDetailDate();
					DetailView inflatedTopRowView = (DetailView) inflater
							.inflate(R.layout.row_details_day, null);
					TextView detailTopRowDate = (TextView) inflatedTopRowView
							.findViewById(R.id.detailTopRowDate);
					detailTopRowDate.setText(details.getFormattedDetailDate());
					View detailTopRowAmountLayout = inflatedTopRowView
							.findViewById(R.id.detailTopRowAmountLayout);
					detailTopRowAmountLayout.setVisibility(View.INVISIBLE);
					inflatedTopRowView.setDetailData(details);

					inflatedTopRowView
							.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									rollExpensesHandler.toggleRoll(v);
								}
							});
					result.add(inflatedTopRowView);
				}else{
					DetailView inflatedView = buildSingleRow(category, inflater, details);
					result.add(inflatedView);
					continue;
				}
			}
			
			DetailView inflatedView = buildSimpleRow(category, inflater, details);
			result.add(inflatedView);
		}

		TextView detailsBalance = (TextView) ((Activity) context).findViewById(R.id.detailsBalance);
		detailsBalance.setText(String.valueOf(limit - sumOfDebit));	
		
		return result;
	}

	private boolean isNewDate(Calendar previousDate, Calendar detailDate) {
		if(previousDate == null){
			return true;
		}
		if(previousDate.get(Calendar.DATE) < detailDate.get(Calendar.DATE)){
			return true;
		}
		return false;
	}

	private DetailView buildSimpleRow(final Category category,
			LayoutInflater inflater, DetailData details) {
		DetailView inflatedView;
		if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			inflatedView = (DetailView) inflater.inflate(R.layout.detail_row, null);
		}else{
			inflatedView = (DetailView) inflater.inflate(R.layout.detail_row_landscape, null);
			TextView expenseCommentTextView = (TextView) inflatedView.findViewById(R.id.expenseCommentTextView);
			expenseCommentTextView.setText(StringControl.cut(details.getComment(), 50));
		}
		TextView detailDate = (TextView) inflatedView.findViewById(R.id.detailDate);
		detailDate.setText(details.getFormattedDetailDate());
		TextView detailAmount = (TextView) inflatedView.findViewById(R.id.detailAmount);
		detailAmount.setText(String.valueOf(details.getAmount()));
		inflatedView.setDetailData(details);
		inflatedView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AbstractPopupWindow expensePopupWindow = new ModifyExpensePopupWindow(
						context, category, (DetailView) v);
				expensePopupWindow.show();
			}
		});
		return inflatedView;
	}
	
	private DetailView buildSingleRow(final Category category,
			LayoutInflater inflater, DetailData details) {
		DetailView inflatedView = (DetailView) inflater.inflate(R.layout.row_details_day, null);
		View detailsTopRowImage = inflatedView.findViewById(R.id.detailsTopRowImage);
		detailsTopRowImage.setVisibility(View.INVISIBLE);
		TextView detailDate = (TextView) inflatedView.findViewById(R.id.detailTopRowDate);
		detailDate.setText(details.getFormattedDetailDate());
		TextView detailAmount = (TextView) inflatedView.findViewById(R.id.detailTopRowAmount);
		detailAmount.setText(String.valueOf(details.getAmount()));
		inflatedView.setDetailData(details);
		inflatedView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AbstractPopupWindow expensePopupWindow = new ModifyExpensePopupWindow(
						context, category, (DetailView) v);
				expensePopupWindow.show();
			}
		});
		return inflatedView;
	}

}
