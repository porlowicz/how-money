package pl.homeproject.expenses.register.data;

import java.util.Calendar;

import pl.homeproject.expenses.persistence.data.Category;

public class ExpenseData {

	private final Calendar date;
	private final int amount;
	private final Category category;
	private final int dataId;
	private final String comment;

	public ExpenseData(int dataId, Category category, Calendar date, int expenseAmountValue) {
		this(dataId, category, date, expenseAmountValue, "");
	}
	
	public ExpenseData(int dataId, Category category, Calendar date, int expenseAmountValue, String comment) {
		this.dataId = dataId;
		this.category = category;
		this.date = date;
		this.amount = expenseAmountValue;
		this.comment = comment;
	}

	public Calendar getDate() {
		return date;
	}

	public int getAmount() {
		return amount;
	}

	public Category getCategory() {
		return category;
	}

	public int getDataId() {
		return dataId;
	}

	public String getComment() {
		return comment;
	}

}
