package pl.homeproject.expenses.register.data;

public class TileOverview {

	private final String category;
	private final String limit;
	private final String debit;
	private final String balance;
	
	public TileOverview(String category, String limit, String debit, String balance){
		this.category = category;
		this.limit = limit;
		this.debit = debit;
		this.balance = balance;
	}
	
	
	public String getCategory() {
		return category;
	}
	public String getLimit() {
		return limit;
	}
	public String getBalance() {
		return balance;
	}
	public String getDebit() {
		return debit;
	}
	
}
