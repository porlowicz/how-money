package pl.homeproject.expenses.view;

import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class CategoryLimitEditText extends EditText {

	private Category category;
	
	public CategoryLimitEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	
	
}
