package pl.homeproject.expenses.view;

import pl.homeproject.expenses.persistence.data.BudgetCategory;
import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.ViewDebug.CapturedViewProperty;

public class CategoryLimitedLengthTextView extends CategoryTextView {

	private Category category;
	private int lengthLimit = 14;

	public CategoryLimitedLengthTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setCategory(Category category) {
		this.category = category;
		setText(category.getLimitedCategoryName(lengthLimit, 2));
		if(this.category instanceof BudgetCategory){
			setTypeface(Typeface.DEFAULT_BOLD);
		}
	}

	public Category getCategory() {
		return category;
	}

	@Override
	@CapturedViewProperty
	public CharSequence getText() {
		return getCategory() != null ? getCategory().getLimitedCategoryName(lengthLimit, 2)
				: "[CATEGORY NAME]";
	}
	
	@Override
	public void setText(CharSequence text, BufferType type) {
		CharSequence categoryName = getCategory() != null ? getCategory()
				.getLimitedCategoryName(lengthLimit, 2) : text;
		if (category instanceof BudgetCategory) {
			super.setText(categoryName.toString().toUpperCase(), type);
		} else {
			super.setText(categoryName, type);
		}
	}
	

	public int getLengthLimit() {
		return lengthLimit;
	}

	public void setLengthLimit(int lengthLimit) {
		this.lengthLimit = lengthLimit;
	}

}
