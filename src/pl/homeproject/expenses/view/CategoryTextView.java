package pl.homeproject.expenses.view;

import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.view.helper.StringControl;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.TextView;

public class CategoryTextView extends TextView{

	private Category category;
	private int lengthLimit = 20;
	
	public CategoryTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
		if (category.getCategory().length() > lengthLimit) {
			setText(StringControl.cut(category.getCategory(), lengthLimit));
		} else {
			setText(category.getCategory());
		}
	}

	@Override
	@CapturedViewProperty
	public CharSequence getText() {
		return getCategory() != null ? getCategory().getCategory() : "[CATEGORY NAME]";
	}
}
