package pl.homeproject.expenses.view;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.KeyboardHider;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class CurrencySpinner extends Spinner implements android.widget.AdapterView.OnItemSelectedListener {

	private int width;
	private int height;
	private EditText replacmentView;
	private Context context;

	public CurrencySpinner(Context context, EditText replacmentView) {
		super(context);
		this.context = context;
		this.replacmentView = replacmentView;
		
		width = replacmentView.getWidth();
		height = replacmentView.getHeight();
		setBackgroundResource(R.drawable.standard_button_selector);
		setPromptId(R.string.currency_prompt);
		String[] objects = new String[]{
				"",
		        "\u20AC",
		        "\u0024",
		        "\u00A3",
		        "\u00A5",
		        "z\u0142",
		        "Custom"
		};
		SpinnerAdapter adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, objects);
		setAdapter(adapter);
		
		setOnItemSelectedListener(this);
	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		setMeasuredDimension(width, height);
		setMinimumHeight(height);
		setMinimumWidth(width);
	}


	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		ArrayAdapter<String> labelAdapter = (ArrayAdapter<String>) parent.getAdapter();
		String labelText = labelAdapter.getItem(position);
		if("".equals(labelText)){
			return;
		}
		if(!"Custom".equals(labelText)){
			replacmentView.setText(labelText);
			replaceSpinnerWithReplacementView();
		}else{
			replaceSpinnerWithReplacementView();
			replacmentView.setText("");
			replacmentView.setHint("Type");
			KeyboardHider.show((Activity) context);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		replaceSpinnerWithReplacementView();
	}

	private void replaceSpinnerWithReplacementView() {
		ViewGroup spinnerParent = (ViewGroup) getParent();
		int indexOfChild = spinnerParent.indexOfChild(this);
		spinnerParent.removeView(this);
		spinnerParent.addView(replacmentView, indexOfChild);
		replacmentView.setSelection(replacmentView.getText().toString().length());
		replacmentView.requestFocus();
	}
}
