package pl.homeproject.expenses.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class DecimalButton extends Button{

	public DecimalButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		StringBuffer buffer = new StringBuffer(text);
		if (text.length() == 0) {
			buffer.insert(0, "000");
		} else if (text.length() == 1) {
			buffer.insert(0, "00");
		} else if (text.length() == 2) {
			buffer.insert(0, "0");
		}
		buffer.insert(buffer.length()-2, ".");
		super.setText(buffer, type);
	}
	
}
