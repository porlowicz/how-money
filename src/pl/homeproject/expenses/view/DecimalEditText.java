package pl.homeproject.expenses.view;

import pl.homeproject.expenses.view.helper.DecimalFormatter;
import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

public class DecimalEditText extends EditText {
	
	public DecimalEditText(Context context){
		super(context);
	}

	public DecimalEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Integer getFormattedText(){
		Editable text = super.getText();
		
		try{
			return Integer.parseInt(DecimalFormatter.removeDecimalDot(text.toString()));
		}catch(NumberFormatException ex){
			return 0;
		}
	}
}
