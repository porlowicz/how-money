package pl.homeproject.expenses.view;

import pl.homeproject.expenses.preferences.Preferences;
import pl.homeproject.expenses.view.helper.DecimalFormatter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.TextView;

public class DecimalTextView extends TextView{

	private Context context;

	public DecimalTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context =  context;
	}

	@Override
	@CapturedViewProperty
	public CharSequence getText() {
		CharSequence text = super.getText();
		String strText = DecimalFormatter.removeDecimalDot(text.toString());
		return strText;
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		String currencySymbol = "";
		if(!isInEditMode()){
			currencySymbol = Preferences.getCurrencySymbol(context);
		}
		super.setText(DecimalFormatter.formatDecimalNumber(text.toString()) + currencySymbol, type);
	}
}
