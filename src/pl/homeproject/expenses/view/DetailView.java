package pl.homeproject.expenses.view;

import pl.homeproject.expenses.register.data.DetailData;
import android.content.Context;
import android.util.AttributeSet;

public class DetailView extends android.support.v7.widget.GridLayout{

	private DetailData detailData;

	public DetailView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setColumnCount(2);
	}

	public DetailData getDetailData() {
		return detailData;
	}

	public void setDetailData(DetailData details) {
		this.detailData = details;
	}

}
