package pl.homeproject.expenses.view;

import pl.homeproject.expenses.MainActivity;
import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.DndScrollHelper;
import pl.homeproject.expenses.persistence.Persister;
import pl.homeproject.expenses.persistence.data.BudgetCategory;
import pl.homeproject.expenses.persistence.data.Category;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ScrollView;
import android.widget.TextView;

public class DndFrameLayoutAdapter extends View implements OnGestureListener{

	private int x;
	private int y;
	private Rect clipBounds = new Rect();
	
	private View draggedView;
	private ScrollView scrollView;
	
	private DndScrollHelper dndScrollHelper;
	
	private GestureDetectorCompat mDetector;
	private Bitmap bitmap;
	private Canvas c;
	private Context context;
	private View enteredBudgetFolder;
	
	public DndFrameLayoutAdapter(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		setClickable(true);
		setLongClickable(true);

		mDetector = new GestureDetectorCompat(context, this);
		mDetector.setIsLongpressEnabled(true);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		ViewGroup parent = (ViewGroup) getParent();
		ViewGroup grandParent = (ViewGroup) parent.getParent();
		
		View rootView = parent.getRootView();
		if(rootView != null){
			int measuredHeight = rootView.findViewById(R.id.tilesGrid).getMeasuredHeight();
			if(measuredHeight < grandParent.getMeasuredHeight()){
				setMeasuredDimension(grandParent.getMeasuredWidth(), grandParent.getMeasuredHeight());
			}else{
				setMeasuredDimension(grandParent.getMeasuredWidth(), measuredHeight);
			}
		}else{
			setMeasuredDimension(grandParent.getMeasuredWidth(), grandParent.getMeasuredHeight());
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(getDraggedView() != null){
			canvas.getClipBounds(clipBounds);
			getDraggedView().layout(0, 0, c.getWidth(), c.getHeight());
			bitmap.eraseColor(0);
			getDraggedView().draw(c);

			int width = getDraggedView().getMeasuredWidth();
			
			canvas.drawBitmap(bitmap, x-width/2, y-width + scrollView.getScrollY(), null);
			
		}
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	    super.onSizeChanged(w, h, oldw, oldh);

	    if(getHeight() == 0){
	    	bitmap = Bitmap.createBitmap(getWidth(), 1, Bitmap.Config.ARGB_8888);
	    }else{
	    	bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
	    }
	    c = new Canvas(bitmap);
	}
	
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
	}

	synchronized public View getDraggedView() {
		return draggedView;
	}

	synchronized public void setDraggedView(View draggedView) {
		this.draggedView = draggedView;
		if(draggedView != null){
			dndScrollHelper = new DndScrollHelper((ScrollView) ((Activity) context).findViewById(R.id.topLevelView));
			this.scrollView = (ScrollView) draggedView.getRootView().findViewById(R.id.topLevelView);
			
			((MainActivity)context).replaceAddTileWithDelete();
			
		}else{
			if(dndScrollHelper != null){
				dndScrollHelper.stopScrolling();
			}
			this.scrollView = null;
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		
		int currentX = (int) e.getRawX();
		int currentY = (int) e.getRawY();
		setPosition(currentX, currentY);
		
		ViewGroup rootView = (ViewGroup) getRootView();
		final GridLayout tilesGrid = (GridLayout) rootView.findViewById(R.id.tilesGrid);
		
		if(e.getAction() == MotionEvent.ACTION_UP){
			enteredBudgetFolder = null;
			if(getDraggedView() != null){
				isTileDroppedOnTarget();
				getDraggedView().setVisibility(View.VISIBLE);
				getDraggedView().requestLayout();
				postInvalidate();
				((MainActivity)context).replaceDeleteWithAddTile();
				setDraggedView(null);
				LockableScrollView topLevelView = (LockableScrollView) rootView.findViewById(R.id.topLevelView);
				topLevelView.setScrollingEnabled(true);
			}else{
				
			}
		}else if(getDraggedView() != null){
			Rect r;
			int[] location = new int[2];
			for(int i = 0; i < tilesGrid.getChildCount(); i++){
				View child = ((ViewGroup)tilesGrid.getChildAt(i));
				if(child == getDraggedView()){
					continue;
				}
				r = new Rect(child.getLeft(), child.getTop(), child.getRight(), child.getBottom());
				child.getLocationInWindow(location);
				r = new Rect(location[0], location[1], location[0] + child.getWidth(), location[1] + child.getWidth());
				if(r.contains(x, y)){
					
					TileView draggedTileTemplateView = (TileView) ((ViewGroup) getDraggedView()).getChildAt(0);
					final TileView targetTileTemplateView = (TileView) ((ViewGroup) child).getChildAt(0);
					pl.homeproject.expenses.view.CategoryLimitedLengthTextView tileCategory = (pl.homeproject.expenses.view.CategoryLimitedLengthTextView) draggedTileTemplateView.findViewById(R.id.tileCategory);
					tileCategory = (pl.homeproject.expenses.view.CategoryLimitedLengthTextView) targetTileTemplateView.findViewById(R.id.tileCategory);
					if(((ViewGroup)targetTileTemplateView).getChildCount() > 0 
							&&
							!(targetTileTemplateView.getParent() instanceof GraphFrameLayoutAdapter) 
							&&
							((targetTileTemplateView.isBudget() && draggedTileTemplateView.isBudget()) 
							|| (!targetTileTemplateView.isBudget() && !draggedTileTemplateView.isBudget()))){
						View draggedParent = getDraggedView();
						View targetParent = (View) targetTileTemplateView.getParent();

						final int indexOfTarget = tilesGrid.indexOfChild(targetParent);
						final int indexOfDragged = tilesGrid.indexOfChild(draggedParent);
						
						final View dp = draggedParent;
						final View tp = targetParent;
						
						tilesGrid.post(new Runnable() {
							
							@Override
							public void run() {
								tilesGrid.removeView(dp);
								tilesGrid.addView(dp, indexOfTarget);
								tilesGrid.removeView(tp);
								tilesGrid.addView(tp, indexOfDragged);
							}
						});
					}else if(((ViewGroup)targetTileTemplateView).getChildCount() > 0 
							&&
							!(targetTileTemplateView.getParent() instanceof GraphFrameLayoutAdapter) 
							&&
							((targetTileTemplateView.isBudget() && !draggedTileTemplateView.isBudget()) )){

						if(targetTileTemplateView != enteredBudgetFolder){
							enteredBudgetFolder = targetTileTemplateView;
							
							Animation animation = new RotateAnimation(-50, 50, targetTileTemplateView.getWidth()/2.0f, targetTileTemplateView.getWidth()/2.0f);
							animation.setInterpolator(new BounceInterpolator());
							animation.setDuration(1000);
							targetTileTemplateView.startAnimation(animation);
							
						}else{
							enteredBudgetFolder = null;
						}
					}
				}
			}
			View scrollUpHandler = rootView.findViewById(R.id.scrollUpHelper);
			scrollUpHandler.getLocationInWindow(location);
			r = new Rect(location[0], location[1], location[0] + scrollUpHandler.getWidth(), location[1] + scrollUpHandler.getHeight());
			if (r.contains(x, y)) {
				dndScrollHelper.startScrolling();
				dndScrollHelper.setStep(-0.5f);
			}else{
				View scrollDownHandler = rootView.findViewById(R.id.scrollDownHelper);
				scrollDownHandler.getLocationInWindow(location);
				r = new Rect(location[0], location[1], location[0] + scrollDownHandler.getWidth(), location[1] + scrollDownHandler.getHeight());
				if(r.contains(x, y)){
					dndScrollHelper.startScrolling();
					dndScrollHelper.setStep(0.5f);
				}else{
					dndScrollHelper.stopScrolling();
				}
			}
			postInvalidate();
		}
		mDetector.onTouchEvent(e);
		
		return tilesGrid.onTouchEvent(e);
	}

	private boolean isTileDroppedOnTarget() {
		View underlyingView = getUnderlyingView();
		if(underlyingView == null){
			return false;
		}
		if(underlyingView.findViewById(R.id.deleteTileTemplateView) != null){
			if(underlyingView instanceof BackDeleteTileFrameLayoutAdapter){
				if(isBack((BackDeleteTileFrameLayoutAdapter) underlyingView, x, y)){
					moveFromFolder(getDraggedView());
					return true;
				}else{
					removeTile(getDraggedView());
					return true;
				}
			}else{
				removeTile(getDraggedView());
				return true;
			}
		}else{
			Category draggedCategory = ((CategoryLimitedLengthTextView)getDraggedView().findViewById(R.id.tileCategory)).getCategory();
			
			if(!(draggedCategory instanceof BudgetCategory)){

				View tileCategory = underlyingView
						.findViewById(R.id.tileCategory);
				if (tileCategory instanceof CategoryLimitedLengthTextView) {
					CategoryLimitedLengthTextView typedTileCategory = (CategoryLimitedLengthTextView) tileCategory;
					if (typedTileCategory.getCategory() instanceof BudgetCategory) {
						Category budgetName = typedTileCategory.getCategory();
						Persister persister = Persister.getInstance(context);
						persister.updateCategoryWith(draggedCategory, budgetName);
						((MainActivity) context).updateTiles();
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isBack(BackDeleteTileFrameLayoutAdapter v,
			int x2, int y2) {
		int height = v.getHeight();
		
		int[] location = new int[2];
		v.getLocationInWindow(location);
		
		if((x2 - location[0])/(height-(y2 - location[1])) < 1.0){
			return true;
		}
		return false;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		View clickedView = getUnderlyingView();
		if(clickedView != null){
			((ViewGroup)clickedView).getChildAt(0).performClick();
		}
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		ViewGroup rootView = (ViewGroup) getRootView();
		
		LockableScrollView topLevelView = (LockableScrollView) rootView.findViewById(R.id.topLevelView);
		topLevelView.setScrollingEnabled(false);

		setDraggedView(null);
		View underlyingView = getUnderlyingView();
		if(underlyingView == null){
			return;
		}
		if(((ViewGroup) ((ViewGroup)underlyingView).getChildAt(0)).getChildCount() > 0){
			setDraggedView(underlyingView);
			getDraggedView().setVisibility(View.INVISIBLE);
			postInvalidate();
		}
	}

	private View getUnderlyingView() {
		ViewGroup rootView = (ViewGroup) getRootView();
		GridLayout tilesGrid = (GridLayout) rootView.findViewById(R.id.tilesGrid);
		
		Rect r;
		int[] location = new int[2];
		View underlyingView = null;		
		for(int i = 0; i < tilesGrid.getChildCount(); i++){
			View v = ((ViewGroup) tilesGrid.getChildAt(i)).getChildAt(0);
			v.getLocationInWindow(location);
			r = new Rect(location[0], location[1], location[0] + v.getWidth(), location[1] + v.getWidth());
			if(r.contains(x, y)){
				underlyingView = (View) v.getParent();
				break;
			}
		}
		return underlyingView;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}
	private void moveFromFolder(View draggedView) {
		CategoryTextView tileCategory = (CategoryTextView) draggedView.findViewById(R.id.tileCategory);
		final Category category = tileCategory.getCategory();
		final Category parentCategory = category.getParentCategory();
		
		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					Category updatedCategory = new Category(category.getId(), category.getCategory(), new Category(1, "DEFAULT", 0), 0);
					
					Persister.getInstance(context).updateCategory(updatedCategory);
					((MainActivity) context).updateTiles();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					((MainActivity) context).updateTiles();
					break;
				}
			}
		};

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		alertDialogBuilder
				.setMessage(String
						.format("Remove %s category from the folder %s?\nYou'll find it afterwards on the start screen.",
								category.getCategory(), parentCategory.getCategory()));
		alertDialogBuilder.setPositiveButton("Yes, remove it", clickListener)
				.setNegativeButton("No, leave it", clickListener);
		alertDialogBuilder.show();
		
	}
	
	private void removeTile(final View view) {
		TextView tileCategory = (TextView) view
				.findViewById(R.id.tileCategory);

		DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					((MainActivity) context).removeCategory(view);
					((MainActivity) context).updateTiles();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					break;
				}
			}
		};
		
		TileView tileTemplateView = (TileView) view.findViewById(R.id.tileTemplateView);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		String folderOrCategory = "category";
		if(tileTemplateView != null){
			if(tileTemplateView.isBudget()){
				folderOrCategory = "folder";
			}
		}
		builder.setMessage(String.format("Delete the %s \"%s\"?",
				folderOrCategory, tileCategory.getText()));
		builder.setPositiveButton("Yes, delete it", clickListener)
				.setNegativeButton("No, leave it", clickListener).show();
	}
}
