package pl.homeproject.expenses.view;

import java.util.Calendar;

import pl.homeproject.expenses.fragment.DateHost;
import pl.homeproject.expenses.register.data.DateFormat;
import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextDateHostAdapter extends EditText implements DateHost {

	public EditTextDateHostAdapter(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EditTextDateHostAdapter(Context context) {
		super(context);
	}

	@Override
	public void setDate(Calendar date) {
		setText(DateFormat.formatDate(date.getTime()));
	}

	@Override
	public Calendar getDate() {
		String dateString = getText().toString();
		return DateFormat.parseDate(dateString);
	}

}
