package pl.homeproject.expenses.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;

public class FrameLayoutAdapter extends FrameLayout{

	public FrameLayoutAdapter(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		
//		int width = getMeasuredWidth();
		View parent = (View) getParent();
		int width = parent.getMeasuredWidth();
		int x = (width * 4) / 9;
		int y = width / 40;
		
		int heightDimension = (int)(x*0.9f);
		setMeasuredDimension(x, heightDimension);
		setMinimumHeight(heightDimension);
		setMinimumWidth(x);

		MarginLayoutParams margins = MarginLayoutParams.class
				.cast(getLayoutParams());
		int margin = y;
		margins.topMargin = margin;
		margins.leftMargin = margin + margin/2;
		
		setLayoutParams(margins);
		
		super.onMeasure(MeasureSpec.makeMeasureSpec(x, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(heightDimension, MeasureSpec.EXACTLY));
	}
}
