package pl.homeproject.expenses.view;

import org.achartengine.GraphicalView;
import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.DialChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DialRenderer;

import android.content.Context;
import android.util.AttributeSet;

public class GraphView extends GraphicalView {
	
	private static CategorySeries dataset = new CategorySeries("");
	private static DialRenderer renderer = new DialRenderer();

	public GraphView(Context context, AttributeSet attributeSet){
		super(context, new DialChart(dataset, renderer));
		for(int i = 0; i < 100; i++){
			dataset.add(i);
		}
		refreshDrawableState();
	}
	
	public GraphView(Context context, AbstractChart abstractChart) {
		super(context, abstractChart);
	}

}
