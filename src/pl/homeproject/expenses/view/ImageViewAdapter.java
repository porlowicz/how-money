package pl.homeproject.expenses.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;

public class ImageViewAdapter extends ImageView{

	public ImageViewAdapter(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		ViewGroup parent = (ViewGroup) getParent().getParent().getParent();
		int width = parent.getMeasuredWidth();		
		
		int x = (int) (width * 0.25*0.5);
		setMeasuredDimension(x, x);
		setMinimumHeight(x);
		setMinimumWidth(x);
		
		MarginLayoutParams margins = MarginLayoutParams.class
				.cast(getLayoutParams());
		int margin = (int) (x*0.25);
		margins.topMargin = margin;
		margins.rightMargin = margin;
		
		setLayoutParams(margins);
	}
	
}
