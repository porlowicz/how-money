package pl.homeproject.expenses.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ScrollView;

public class LegendScrollView extends ScrollView{

	public LegendScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
//		super.onMeasure(getMeasuredWidth(), getMeasuredHeight());
		
		ViewGroup parent = (ViewGroup) getParent();
		int measuredHeight = parent.getMeasuredHeight();
		
		setMeasuredDimension(parent.getMeasuredWidth(), measuredHeight/4);
		setMinimumHeight(measuredHeight/4);
		setMinimumWidth(parent.getMeasuredWidth());
		
	}
	
}
