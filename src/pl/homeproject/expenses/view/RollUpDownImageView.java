package pl.homeproject.expenses.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RollUpDownImageView extends ImageView {

	private enum RollUpState {
		UP, UP_HALF, DOWN;
	}

	private RollUpState state = RollUpState.DOWN;

	public RollUpDownImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public boolean isUp() {
		return state == RollUpState.UP;
	}

	public boolean isUpHalf() {
		return state == RollUpState.UP_HALF;
	}

	public boolean isDown() {
		return state == RollUpState.DOWN;
	}

	public void nextState() {
		switch (state) {
		case UP:
			state = RollUpState.DOWN;
			break;
		case UP_HALF:
			state = RollUpState.UP;
			break;
		case DOWN:
			state = RollUpState.UP_HALF;
			break;
		default:
			break;
		}
	}
}
