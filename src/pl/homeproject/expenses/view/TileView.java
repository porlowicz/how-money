package pl.homeproject.expenses.view;

import pl.homeproject.expenses.R;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TileView extends android.support.v7.widget.GridLayout {

	private boolean isBudget = false;
	
	public TileView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setColumnCount(1);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		
//		int width = getMeasuredWidth();
		int width = MeasureSpec.getSize(widthMeasureSpec);
//		int x = (width * 4) / 9;
		int x = width;
		int heightDimension = (int)(x*0.9f);
		setMeasuredDimension(x, heightDimension);
		setMinimumHeight(heightDimension);
		setMinimumWidth(x);
		
		super.onMeasure(MeasureSpec.makeMeasureSpec(x, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(heightDimension, MeasureSpec.EXACTLY));
	}

	public boolean isBudget() {
		return isBudget;
	}

	public void setBudget(boolean isBudget) {
		this.isBudget = isBudget;
		if(isBudget){
			TextView tileCategory = (TextView) findViewById(R.id.tileCategory);
			tileCategory.setPadding(0, 10, 0, 0);
			tileCategory.setTypeface(Typeface.DEFAULT_BOLD);
		}
	}

}
