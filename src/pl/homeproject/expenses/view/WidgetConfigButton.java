package pl.homeproject.expenses.view;

import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.Button;
import android.widget.TextView;

public class WidgetConfigButton extends Button{
		
		private String label = "";
		private Category data;
		public WidgetConfigButton(Context context, AttributeSet attributeSet) {
			super(context, attributeSet);
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
			setText(label);
		}
		public Category getData() {
			return data;
		}
		public void setData(Category data) {
			this.data = data;
		}
		
		@Override
		public void setText(CharSequence text, BufferType type) {
			super.setText(label, type);
		}
		
		@Override
		@CapturedViewProperty
		public CharSequence getText() {
			return label;
		}
		
}
