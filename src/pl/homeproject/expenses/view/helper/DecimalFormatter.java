package pl.homeproject.expenses.view.helper;

public class DecimalFormatter {

	public static String removeDecimalDot(String text){
		StringBuffer string = new StringBuffer(text);
		int indexOfSeparator = string.indexOf(".");
		if (indexOfSeparator < 0) {
			string.append("00");
		} else if (indexOfSeparator == string.length() - 1) {
			string.replace(string.length() - 1, string.length(), "00");
		} else if (indexOfSeparator == string.length() - 2) {
			string.replace(string.length() - 2, string.length() - 1, "");
			string.append("0");
		} else if (indexOfSeparator == string.length() - 3) {
			string.replace(string.length() - 3, string.length() - 2, "");
		} else {
			string = new StringBuffer(string.substring(0, indexOfSeparator + 3));
			string.replace(string.length() - 3, string.length() - 2, "");
		}
		return string.toString().replace(" ", "");
	}
	
	public static String formatDecimalNumber(int number){
		return formatDecimalNumber(String.valueOf(number));
	}
	
	public static String formatDecimalNumber(String text){
		StringBuffer buffer = new StringBuffer(text);
		if (text.length() == 0) {
			buffer.insert(0, "000");
		} else if (text.length() == 1) {
			buffer.insert(0, "00");
		} else if (text.length() == 2) {
			buffer.insert(0, "0");
		}
		buffer.insert(buffer.length()-2, ".");
		int commaPosition = buffer.indexOf(".") - 3;
		while(commaPosition > 0){
			buffer.insert(commaPosition, " ");
			commaPosition -= 3;
		}
		
		return buffer.toString();
	}
}
