package pl.homeproject.expenses.view.helper;

public class StringControl {

	public static String cut(String category, int i) {
		if (category.length() > i) {
			return category.substring(0, i) + "...";
		}
		return category;
	}

	public static String firstLetterUpperCase(String category) {
		if(category == null || category.length() == 0){
			return category;
		}
		return category.substring(0,1).toUpperCase() + category.substring(1).toLowerCase();
	}

}
