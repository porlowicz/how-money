package pl.homeproject.expenses.view.model;

import pl.homeproject.expenses.R;
import pl.homeproject.expenses.handler.SubMainDisplay;
import pl.homeproject.expenses.persistence.data.BudgetCategory;
import pl.homeproject.expenses.persistence.data.Category;
import android.content.Context;
import android.graphics.Color;
import android.view.View.OnClickListener;



public class BudgetTileViewModel extends TileViewModel{

	public BudgetTileViewModel(Category budget, int limit, int debit, int orderNumber) {
		super(budget, limit, debit, orderNumber);
	}

	@Override
	public int getBackgroundColor() {
		return Color.TRANSPARENT;
	}
	
	@Override
	public int getBackgroundResource() {
		return R.drawable.folder_with_logo;
	}

	@Override
	public OnClickListener getOnClickHandler(Context context) {
		return new SubMainDisplay(context, getCategory());
	}
	
}
