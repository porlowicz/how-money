package pl.homeproject.expenses.view.model;

import pl.homeproject.expenses.handler.DetailsDisplay;
import pl.homeproject.expenses.persistence.data.Balance;
import pl.homeproject.expenses.persistence.data.Category;
import pl.homeproject.expenses.register.TileFactory;
import android.content.Context;
import android.view.View.OnClickListener;

public class TileViewModel {

	final private int limit;
	final private int debit;
	final private int balance;
	final private int backgroundColor;
	final private Category category;
	final private int tileOrdinalNumber;
	
	
	public String getPercentage(){
		String result = "0%";
		if(limit != 0){
			result = String.format("%s%%", debit * 100 / limit);
		}
		if(result.length() > 4){
			return "...";
		}
		return result;
	}
	
	public TileViewModel(Category categoryName, int limit, int debit, int ordinalNumber) {
		this.category = categoryName;
		this.limit = limit;
		this.debit = debit;
		this.balance = limit - debit;
		this.backgroundColor = TileFactory.gradientFor(new Balance(limit, debit));
		this.tileOrdinalNumber = 1;
	}
	
	public int getDebit() {
		return debit;
	}

	public int getBalance() {
		return balance;
	}

	public int getBackgroundColor() {
		return backgroundColor;
	}
	
	public int getBackgroundResource(){
		return pl.homeproject.expenses.R.drawable.page_with_logo;
	}

	public String getTileLabel() {
		return category.getCategory();
	}

	public int getLimit() {
		return limit;
	}
	
	public OnClickListener getOnClickHandler(Context context){
		return new DetailsDisplay(context, category);
	}
	
	public String getParentBudget(){
		return category.getParentCategory().getCategory();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof TileViewModel){
			TileViewModel tileViewModel = (TileViewModel) o;
			return getTileLabel().equals(tileViewModel.getTileLabel());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getTileLabel().hashCode();
	}

	public Category getCategory() {
		return category;
	}

	public int getTileOrdinalNumber() {
		return tileOrdinalNumber;
	}
}
